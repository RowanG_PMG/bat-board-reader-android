package com.pinkzebra.batboardreader

import androidx.test.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import com.pinkzebra.batboardreader.models.SingleBoardCaptureData
import com.pinkzebra.batboardreader.models.Prefs
import com.pinkzebra.batboardreader.models.ProductPrice

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getTargetContext()
        assertEquals("com.pinkzebra.batboardreader", appContext.packageName)
    }

    @Test
    fun setProductPrices() {
        val appContext = InstrumentationRegistry.getTargetContext()

        val prefs = Prefs(appContext)
        val productPrices : List<ProductPrice> = listOf(ProductPrice(productName = "Peter Stuy", productSize = "25s", price = 3.44f, packageType = ""),
            ProductPrice(productName = "Rothmans", productSize = "30s", price = 5.66f, packageType = "pack")
            )
        val testProducts = SingleBoardCaptureData(itemData = productPrices.toMutableList(), boardImageUriString = "", timestamp = "", venueName = "", boardType = "")

        prefs.updateProductPrices(testProducts)
    }

    @Test
    fun getProductPrices() {
        val appContext = InstrumentationRegistry.getTargetContext()

        val prefs = Prefs(appContext)

        val productPrices = prefs.getProductPrices()
        print(productPrices)
    }
}
