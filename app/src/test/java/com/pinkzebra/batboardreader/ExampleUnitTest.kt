package com.pinkzebra.batboardreader

import com.pinkzebra.batboardreader.models.BoardReaderAlgorithm
import com.pinkzebra.batboardreader.utils.convertTimeZoneStampToUnixTimeStamp
import org.junit.Test
import org.junit.Assert.*
import java.lang.Math.abs

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    //Pairs in center x, center y format
    val lineItem1 = Pair(0.0, 0.0)
    val lineItem2 = Pair(0.0, 1.0)
    val lineItem3 = Pair(0.0, 2.0)
    val lineItem4 = Pair(0.0, 3.0)
    val lineItem5 = Pair(0.0, 4.0)
    val lineItem6 = Pair(1.0, 0.0)
    val lineItem7 = Pair(3.0, 4.0)

    var items: List<Int> = listOf(5, 6)

    val allLineItems: MutableList<Pair<Double, Double>> =
            mutableListOf(lineItem1, lineItem2, lineItem3, lineItem4, lineItem5, lineItem6, lineItem7)

    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun testSubStringsDoubleDollar() {
        val myString = "$12.30 $23.50"

        var indexesOfDollarFound = mutableListOf<Int>()

        var countOfDollarsInText: Int = 0
        myString.forEachIndexed { index, char ->
            if (char == '$') {
                countOfDollarsInText++
                indexesOfDollarFound.add(index)
            }
        }

        println(countOfDollarsInText)
        println(indexesOfDollarFound)

        val packPriceString = myString.substring(0, indexesOfDollarFound[1])
        println(packPriceString)

        val packPrice = packPriceString.replace("$", "", ignoreCase = true).toFloatOrNull()
        println(packPrice)

        val cartonPriceString = myString.substring(indexesOfDollarFound[1])
        println(cartonPriceString)

        val cartonPrice = cartonPriceString.replace("$", "", ignoreCase = true).toFloatOrNull()
        println(cartonPrice)
    }

    @Test
    fun testGetBoardTextItemModelFromSingleStringBoardType3() {

        val string = "Craftsman 15g RYO \$22.94"

        val detectedBoardTextItem = BoardReaderAlgorithm(null).withSingleStringProcessBoardType3(string)

        detectedBoardTextItem
    }

    @Test
    fun testConversionDate() {
       val pastTimestamp =  "2019-9-01T01:18:57.098Z"
        val currentTimestamp =  "2019-10-15T01:18:57.098Z"
        val convertedTimeOfPastStamp = convertTimeZoneStampToUnixTimeStamp(pastTimestamp)
        val convertedCurrentTimeStamp = convertTimeZoneStampToUnixTimeStamp(currentTimestamp)

        val timeDiff = (convertedCurrentTimeStamp - convertedTimeOfPastStamp)/1000

        if (timeDiff <= 60 * 24 * 60 * 60L) {

        } else {
            fail()
        }
    }

    @Test
    fun listsWorking() {
       items = listOf(7, 8)
//        var item = items[1]
//        item = 9
//        items[1] = 9

        print(items)
    }

    @Test
    fun testDecimals() {
        val text = "he.ll.o."
        val indexOfLastDecimal = text.indexOfLast {
            it == '.'
        }
        print("Index of last " + indexOfLastDecimal)
           val trimSecondDecimalString = text.replaceRange(indexOfLastDecimal, indexOfLastDecimal + 1, "")
        print("TrimmedString is:  " + trimSecondDecimalString)
    }


    @Test
    fun makeHorizontalLineItems() {
        val allLinesGroupedByCloseVertical: MutableList<List<Pair<Double, Double>>> = mutableListOf()
        val sorted = sortAllLinesByVerticalAscending(allLineItems)
        println(sorted)


        while (sorted.count() > 0) {
            val firstCloseMatchingVerticalSet = withVerticalSortedGroupGetFirstCloseVerticalMatchSet(sorted)
            if (firstCloseMatchingVerticalSet != null) {
                allLinesGroupedByCloseVertical.add(firstCloseMatchingVerticalSet)
                for (line in firstCloseMatchingVerticalSet) {
                    sorted.remove(line)
                }
            } else {
                sorted.removeAt(0)
            }
        }
        for (lineItem in allLinesGroupedByCloseVertical) {
            println("Line item: " + lineItem)
        }

        //  withFirstLineCheckForVerticalMatchesAndAddToList()
//        for (line in allLineItems) {
//            if line.second
//        }
    }


    fun withVerticalSortedGroupGetFirstCloseVerticalMatchSet(lineItems: List<Pair<Double, Double>>): List<Pair<Double, Double>>? {
        if (lineItems.count() < 2) return null
        val firstLine = lineItems.first()

        val newVerticalLineGroup: MutableList<Pair<Double, Double>> = mutableListOf(firstLine)
        val lineItemCount = lineItems.count()
        for (x in 1..(lineItemCount - 1)) {
            val lineToCheck = lineItems[x]
            if ((abs(lineToCheck.second - firstLine.second)) < 0.1) {
                newVerticalLineGroup.add(lineToCheck)
            } else {
                break
            }
        }
        if (newVerticalLineGroup.count() >= 2) {
            return newVerticalLineGroup
        } else {
            return null
        }
    }

    fun sortAllLinesByVerticalAscending(lineItems: List<Pair<Double, Double>>): MutableList<Pair<Double, Double>> {
        val sortedAllLineItems = lineItems.toMutableList()
        sortedAllLineItems.sortWith(compareBy({ it.second }, { it.first }))
        return sortedAllLineItems
    }

}
