package com.pinkzebra.batboardreader.utils

fun replaceWhiteSpaceWith(text: String, replacement: String): String {
    var replacedWhiteSpaceWithSpaceWord = text
    replacedWhiteSpaceWithSpaceWord = replacedWhiteSpaceWithSpaceWord.replace("&", replacement)
    replacedWhiteSpaceWithSpaceWord = replacedWhiteSpaceWithSpaceWord.replace("-", replacement)
    replacedWhiteSpaceWithSpaceWord = replacedWhiteSpaceWithSpaceWord.replace(",", replacement)
    replacedWhiteSpaceWithSpaceWord = replacedWhiteSpaceWithSpaceWord.replace("+", replacement)
    replacedWhiteSpaceWithSpaceWord = replacedWhiteSpaceWithSpaceWord.replace("'", replacement)
    replacedWhiteSpaceWithSpaceWord = replacedWhiteSpaceWithSpaceWord.replace(".", replacement)
    replacedWhiteSpaceWithSpaceWord = replacedWhiteSpaceWithSpaceWord.replace("(", replacement)
    replacedWhiteSpaceWithSpaceWord = replacedWhiteSpaceWithSpaceWord.replace(")", replacement)
    replacedWhiteSpaceWithSpaceWord = replacedWhiteSpaceWithSpaceWord.replace("/", replacement)
    replacedWhiteSpaceWithSpaceWord = replacedWhiteSpaceWithSpaceWord.replace("|", replacement)

    return replacedWhiteSpaceWithSpaceWord
}