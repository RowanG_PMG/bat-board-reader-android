package com.pinkzebra.batboardreader.utils

import android.net.Uri

fun getUriFromString(uRiString: String): Uri? {
    return Uri.parse(uRiString)
}