package com.pinkzebra.batboardreader.utils

import android.net.Uri
import android.widget.ImageView
import com.squareup.picasso.Picasso
import android.graphics.drawable.Drawable
import android.graphics.Bitmap
import android.util.Log

fun loadImageFromUriIntoView(uRi: Uri, destinationView: ImageView, successHandler: ((imageWidth: Int?, imageHeight: Int?) -> Unit)? = null, failHandler: (() -> Unit)? = null) {
    Picasso.get().load(uRi).into(object : com.squareup.picasso.Target {
        override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
            bitmap?.let {
                val width = bitmap.width
                val height = bitmap.height
                destinationView.setImageBitmap(bitmap)
                successHandler?.invoke(width, height)
            }
        }

        override fun onBitmapFailed(e: java.lang.Exception?, errorDrawable: Drawable?) {
            Log.i("PICASSO", "Failed- scan receipt image load")
        }

        override fun onPrepareLoad(placeHolderDrawable: Drawable?) {

        }
    })
}