package com.pinkzebra.batboardreader.utils

import android.content.Context
import android.media.ExifInterface
import android.net.Uri
import android.provider.MediaStore
import android.util.Log

fun getImageRotation(uri: Uri, context: Context): Int {
//TODO: Figure out better way of getting image orientation from mediastore selected image

    val columns = arrayOf(MediaStore.MediaColumns.DATA)
    val c = context.contentResolver!!.query(uri, columns, null, null, null)
    if (c == null) {
        Log.d("", "Could not get cursor")
        return 0
    }

    c.moveToFirst()
    val idx = c.getColumnIndex(MediaStore.Images.ImageColumns.DATA)

    val str = c.getString(0)

    if (str == null) {
        Log.d("", "Could not get exif")
        return -90
    }
    val cGetString = c.getString(0)!!
    val exifInterface = ExifInterface(cGetString)
    val orientation: Int =
        when (exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED)) {
            ExifInterface.ORIENTATION_ROTATE_90 -> 90
            ExifInterface.ORIENTATION_ROTATE_180 -> 180
            ExifInterface.ORIENTATION_ROTATE_270 -> 270
            else -> 0
        }
    Log.i("Orientation", "Image Orientation from gallery is $orientation")
    c.close()
    return orientation - 90

}