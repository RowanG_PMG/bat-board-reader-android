package com.pinkzebra.batboardreader.utils

import android.app.Activity
import android.content.ContentValues
import android.util.Log
import java.io.File

fun makeDestinationBoardImageFile(activity: Activity): File? {
    val destinationFile = File(
            activity.externalMediaDirs.first(),
            "${System.currentTimeMillis()}.jpg"
    )
    if (!destinationFile.exists()) {
        Log.i(ContentValues.TAG, "Image file created does not exist")
    }
    return destinationFile
}