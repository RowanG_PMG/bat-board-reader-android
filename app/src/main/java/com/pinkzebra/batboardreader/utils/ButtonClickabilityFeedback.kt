package com.pinkzebra.batboardreader.utils

import android.content.Context
import android.view.View
import androidx.core.content.ContextCompat
import com.pinkzebra.batboardreader.R


fun showButtonDisabled(view: View, context: Context) {
    view.setBackgroundColor(ContextCompat.getColor(context, R.color.light_gray))
}

fun showButtonEnabled(view: View, context: Context) {
    view.setBackgroundColor(ContextCompat.getColor(context, R.color.cyan))
}