package com.pinkzebra.batboardreader.utils

import java.security.Timestamp
import java.text.SimpleDateFormat
import java.time.Instant
import java.time.LocalDate
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*

fun convertTimeZoneStampToUnixTimeStamp(TZStamp: String): Long {
    val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    formatter.timeZone = TimeZone.getTimeZone("GMT")
    return formatter.parse(TZStamp).time
}

fun convertTimeZoneStampToUserPrintedSydneyDateAndTimeFormat(TZStamp: String): String {
    val timeStamp = convertTimeZoneStampToUnixTimeStamp(TZStamp)
    return "${convertGMTToSydneyDateString(timeStamp)} @ ${convertGMTToSydneyTimeString(timeStamp)}"
}

fun convertGMTToSydneyDateString(timestamp: Long): String {
    val timeFormat = SimpleDateFormat("dd/MM/yyyy")
    timeFormat.timeZone = TimeZone.getTimeZone("GMT+10:00")
    return timeFormat.format(timestamp)
}

fun convertGMTToSydneyTimeString(timestamp: Long): String {
    val timeFormat = SimpleDateFormat("hh:mm a")
    timeFormat.timeZone = TimeZone.getTimeZone("GMT+10:00")
    return timeFormat.format(timestamp)
}

fun Date.getStringTimeStampWithDate(): String {
    val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
        Locale.getDefault())
    dateFormat.timeZone = TimeZone.getTimeZone("GMT")
    return dateFormat.format(this)
}

fun getCurrentTZTimeStamp(): String {
    val today = Calendar.getInstance().time
    val currentTimeStamp = today.getStringTimeStampWithDate()
    return currentTimeStamp
}

fun getCurrentTZTimeStampMinusTime(daysAgo: Int): String {

    val todayDate = Calendar.getInstance().time
    val todayTime = todayDate.time

    val timeAgo = todayTime - (daysAgo * 24 * 60 * 60 * 1000)
    val timeAgoDate = Date(timeAgo)
    return  timeAgoDate.getStringTimeStampWithDate()
}