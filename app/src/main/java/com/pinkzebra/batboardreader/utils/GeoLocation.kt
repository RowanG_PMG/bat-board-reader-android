package com.pinkzebra.batboardreader.utils

import android.content.ContentValues
import android.util.Log
import com.google.android.gms.maps.model.LatLng

fun distanceBetweenPoints(latLng1: LatLng, latLng2: LatLng): Double {
    val radiusEarth = 6371 // km

    val lat1 = (latLng1.latitude / 180) * Math.PI
    val lat2 = (latLng2.latitude / 180) * Math.PI
    val lon1 = (latLng1.longitude / 180) * Math.PI
    val lon2 = (latLng2.longitude / 180) * Math.PI

    val x = (lon2 - lon1) * Math.cos((lat1 + lat2) / 2)
    val y = lat2 - lat1

   // Log.i(ContentValues.TAG, "LatLng1 is: $latLng1")
  //  Log.i(ContentValues.TAG, "LatLng2 is: $latLng2")

    val distance = Math.sqrt(x * x + y * y) * radiusEarth
    //Log.i(ContentValues.TAG, "Distance is: $distance")
    return Math.sqrt(x * x + y * y) * radiusEarth
}