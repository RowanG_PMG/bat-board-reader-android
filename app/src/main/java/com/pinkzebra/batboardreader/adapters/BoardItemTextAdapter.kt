package com.pinkzebra.batboardreader.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import android.widget.Spinner
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.pinkzebra.batboardreader.models.BoardTextItemModel
import com.pinkzebra.batboardreader.R
import com.pinkzebra.batboardreader.models.Product

import kotlinx.android.synthetic.main.layout_board_text_item.view.*
import java.lang.Math.abs
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener

import android.util.Log


interface BoardItemTextListener {
    fun dropdownItemSelected(productID: Int?, itemIndex: Int)
}

class BoardItemTextAdapter(private val boardTextDataSet: MutableList<BoardTextItemModel>, private val productData: List<Product>, private val listener: BoardItemTextListener? = null, var boardEditable: Boolean = false) :
        RecyclerView.Adapter<BoardItemTextAdapter.BoardTextItemViewHolder>() {

    private var dropdownListMapOfProductIDs: HashMap<Int, List<ProductDropDown>> = hashMapOf()
    private var dropdownSelectedPosition: HashMap<Int, Int> = hashMapOf()

    data class ProductDropDown(val productSize: String, val productID: Int?, val product: Product)

    class BoardTextItemViewHolder(val linearLayout: LinearLayout) : RecyclerView.ViewHolder(linearLayout)

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
    ): BoardTextItemViewHolder {
        // create a new view
        val linearLayout = LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_board_text_item, parent, false) as LinearLayout
        // set the view's size, margins, paddings and layout parameters

        return BoardTextItemViewHolder(linearLayout)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: BoardTextItemViewHolder, position: Int) {
        val boardPosition = position
        boardTextDataSet[position].let { boardTextItemModel ->

            if (boardTextItemModel.isCompliant == false) {
                holder.linearLayout.product_name.setTextColor(ContextCompat.getColor(holder.linearLayout.context, R.color.red))
            } else {
                holder.linearLayout.product_name.setTextColor(ContextCompat.getColor(holder.linearLayout.context, R.color.black))
            }


            holder.linearLayout.product_name.text = boardTextItemModel.productName
            val priceWithTwoDecimal = "%.2f".format(boardTextItemModel.productPrice)
            holder.linearLayout.product_price.text = "$${priceWithTwoDecimal}"
            holder.linearLayout.product_size.text = boardTextItemModel.productSize
            holder.linearLayout.product_package.text = "${boardTextItemModel.packageType}"
            var priceDiffText: String = ""

            boardTextItemModel.priceDifference?.let {
                val priceDiffPercen = it / (boardTextItemModel.productPrice) * 100
                if (abs(priceDiffPercen) > 10) {
                    holder.linearLayout.product_price_difference_percent.setBackgroundColor(ContextCompat.getColor(holder.linearLayout.context, R.color.yellow))
                } else {
                    holder.linearLayout.product_price_difference_percent.setBackgroundColor(ContextCompat.getColor(holder.linearLayout.context, R.color.white))
                }
                val formattedDecimalPriceDiffAbsolute = "%.2f".format(abs(it))
                priceDiffText = "$${formattedDecimalPriceDiffAbsolute}"
                if (it < 0) {
                    priceDiffText = "-" + priceDiffText
                }
                if (it >= 0) {
                    holder.linearLayout.product_price_difference_percent.setTextColor(ContextCompat.getColor(holder.linearLayout.context, R.color.green))
                } else {
                    holder.linearLayout.product_price_difference_percent.setTextColor(ContextCompat.getColor(holder.linearLayout.context, R.color.red))
                }
            } ?: run {
                priceDiffText = "-"
                holder.linearLayout.product_price_difference_percent.setTextColor(ContextCompat.getColor(holder.linearLayout.context, R.color.black))
                holder.linearLayout.product_price_difference_percent.setBackgroundColor(ContextCompat.getColor(holder.linearLayout.context, R.color.white))
            }

            holder.linearLayout.product_price_difference_percent.text = priceDiffText

            val context = holder.linearLayout.product_selector.context
            val productDataString = productData.map {
                "${it.name} ${it.size}"
            }

            val arrayListProducts = ArrayList<String>()
            arrayListProducts.addAll(productDataString)
            if (boardEditable) {
                if (boardTextItemModel.wasAutoMapped == false || boardTextItemModel.wasAutoMapped == null) {
                    populateDropdownProductSelector(context, holder.linearLayout.product_selector, boardTextItemModel.productName, position)

                    holder.linearLayout.product_selector.post {
                        val productID = boardTextItemModel.productID
                        productID?.let {
                            val dropdownSelectedPosition = getProductSelectorPosition(it, boardPosition)
                            dropdownSelectedPosition?.let {
                                holder.linearLayout.product_selector.setSelection(it)
                            }
                        }

                    }

                    holder.linearLayout.product_selector.setOnItemSelectedListener(object : OnItemSelectedListener {
                        override fun onItemSelected(parentView: AdapterView<*>, selectedItemView: View, position: Int, id: Long) {
                            val selectorPosition = position
                            //   Log.i("Product", "Dropdown item selected at position $selectorPosition")
                            val item = parentView.getItemAtPosition(position).toString()

                            //  Log.i("Product", "item is $item for board position: $boardPosition")
                            val productsForDropdown = dropdownListMapOfProductIDs[boardPosition]
                                    ?: listOf()
                            //Bug? in Android gives false position with dropdown inside Recycler.
                            val indexSelectedItem = productsForDropdown.indexOfFirst { it.productSize == item }

                          //  Log.i("Product", "Dropdown item selected at position $indexSelectedItem")

                            indexSelectedItem?.let {
                                val selectedProduct = productsForDropdown[position]

                                //  boardTextDataSet[position].productID = selectedProduct.productID
                                dropdownSelectedPosition[boardPosition] = indexSelectedItem
                                listener?.dropdownItemSelected(selectedProduct.productID, boardPosition)
                            }

                            // resetRowRemoveSelector(holder.linearLayout)
                        }

                        override fun onNothingSelected(parentView: AdapterView<*>) {
                            Log.i("Product", "No Item selected")
                            //  resetRowRemoveSelector(holder.linearLayout)
                        }
                    })

                    //   holder.linearLayout.product_name.setOnClickListener {
                    holder.linearLayout.product_selector.visibility = View.VISIBLE
                    holder.linearLayout.product_name.visibility = View.GONE
                    holder.linearLayout.product_size.visibility = View.GONE
                    //  }

                } else {
                    hideProductSelector(holder)
                }
            } else {
                hideProductSelector(holder)
            }
        }
    }

    private fun getProductSelectorPosition(productID: Int, boardPosition: Int): Int? {
        val productsForDropdown = dropdownListMapOfProductIDs[boardPosition]
                ?: return null
        val indexSelectedItem = productsForDropdown.indexOfFirst { it.productID == productID }
        return indexSelectedItem
    }

    private fun hideProductSelector(holder: BoardTextItemViewHolder) {
        holder.linearLayout.product_selector.visibility = View.GONE
        holder.linearLayout.product_name.visibility = View.VISIBLE
        holder.linearLayout.product_size.visibility = View.VISIBLE
    }


    private fun resetRowRemoveSelector(linearLayout: LinearLayout) {
        linearLayout.product_selector.visibility = View.GONE
        linearLayout.product_name.visibility = View.VISIBLE
        linearLayout.product_size.visibility = View.VISIBLE
    }

    private fun populateDropdownProductSelector(context: Context, spinner: Spinner, thisProductName: String, boardIndex: Int) {
        val whitespaceStrippedProductName = replaceWhitespaceWithSpaces(thisProductName)
        val csvThisProductName = whitespaceStrippedProductName.split(" ")

        val keywordCountProductData = productData.map {
            val csvArray = it.name.split(" ")
            val keywordCount = csvArray.intersect(csvThisProductName).count()
            Pair(it, keywordCount)
        }

        val sortedByKeyWordCountProducts = keywordCountProductData.sortedWith(compareByDescending<Pair<Product, Int>> { it.second }.thenBy { it.first.name })

        val thisDropDownForProductSelectorWithSelectionPrompt = mutableListOf<ProductDropDown>(ProductDropDown(productID = null, productSize = "Select product", product = Product(name = "", size = "", packageType = "")))
        thisDropDownForProductSelectorWithSelectionPrompt.addAll(
                sortedByKeyWordCountProducts.map {
                    val key = it.first
                    val productWithSize = "${key.name} ${key.size}"
                    ProductDropDown(productID = key.productID, productSize = productWithSize, product = key)
                }
        )

//        val thisDropDownForProductSelector = sortedByKeyWordCountProducts.map {
//            val key = it.first
//            val productWithSize = "${key.name} ${key.size}"
//            ProductDropDown(productID = key.productID, productSize = productWithSize)
//        }

        // val productSizeWithSelectionPrompt = mutableListOf<String>("Select product")
        val productSizeWithSelectionPrompt = (thisDropDownForProductSelectorWithSelectionPrompt.map { it.productSize })

        val arrayAdapter = ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, productSizeWithSelectionPrompt)
        spinner.adapter = arrayAdapter

        dropdownListMapOfProductIDs[boardIndex] = thisDropDownForProductSelectorWithSelectionPrompt
    }

    private fun replaceWhitespaceWithSpaces(word: String): String {
        var whitespaceWord = word.replace("&", " ")
        whitespaceWord = whitespaceWord.replace("-", " ")
        whitespaceWord = whitespaceWord.replace("+", " ")
        return whitespaceWord
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = boardTextDataSet.size

}