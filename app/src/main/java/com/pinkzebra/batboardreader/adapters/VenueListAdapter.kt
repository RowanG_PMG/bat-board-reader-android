package com.pinkzebra.batboardreader.adapters

import android.content.ContentValues.TAG
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.maps.model.LatLng
import com.pinkzebra.batboardreader.R
import kotlinx.android.synthetic.main.layout_board_text_item.view.product_price
import kotlinx.android.synthetic.main.layout_venue_list_item.view.*

data class VenueListModel(
        val venueID: Int,
    val markerID: Int,
    val venueName: String,
    val venueDistance: Float,
    val productPrice: Float?=null,
    var isFavorite: Boolean,
    val address: String,
    val boardType: String? = null,
    val latLng: LatLng
)

interface VenueListItemListener {
    fun cameraCaptureClicked(position: Int)
    fun listItemClicked(position: Int)
}

class VenueListAdapter(
    private val venuesDataSet: MutableList<VenueListModel>,
    private val listener: VenueListItemListener? = null,
    private val showPricing: Boolean = false,
    var selectedVenueIndex: Int? = null
) :
    RecyclerView.Adapter<VenueListAdapter.VenueListItemViewHolder>() {

    class VenueListItemViewHolder(val layout: ConstraintLayout) : RecyclerView.ViewHolder(layout)

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): VenueListItemViewHolder {
        // create a new view
        val layout = LayoutInflater.from(parent.context)
            .inflate(R.layout.layout_venue_list_item, parent, false) as ConstraintLayout
        // set the view's size, margins, paddings and layout parameters
        return VenueListItemViewHolder(layout)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: VenueListItemViewHolder, position: Int) {
        venuesDataSet[position].let {
            holder.layout.venue_name.text = it.venueName
            if (showPricing == true) {
                holder.layout.product_price.visibility = View.VISIBLE
                holder.layout.product_price.text = "$" + "%.2f".format(it.productPrice)
            }

            holder.layout.venue_distance.text = "${it.venueDistance}km"
            holder.layout.venue_board_capture_icon.setOnClickListener {
                listener?.cameraCaptureClicked(position)
            }

            holder.layout.setOnClickListener {
                listener?.listItemClicked(position)
                Log.i(TAG, "Item clicked")

            }

            if (selectedVenueIndex == position) {
                holder.layout.venue_name.setTextColor(ContextCompat.getColor(holder.layout.context, R.color.cyan))
            } else {
                holder.layout.venue_name.setTextColor(ContextCompat.getColor(holder.layout.context, R.color.black))
            }
//            val favoriteImageResource = if (it.isFavorite) R.drawable.favorite_heart_icon_filled else R.drawable.favorite_heart_icon
//            holder.layout.venue_board_capture_icon.setImageResource(favoriteImageResource)
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = venuesDataSet.size

}