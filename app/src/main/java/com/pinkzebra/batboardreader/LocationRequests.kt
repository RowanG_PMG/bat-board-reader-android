package com.pinkzebra.batboardreader

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.location.Location
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.LatLng

const val PERMISSIONS_REQUEST_LOCATION_VENUE = 12345
val sydneyLocation = LatLng(-33.8688, 151.2093)

class LocationRequests(private val activity: Activity, private val fragment: Fragment, private val locationReceivedHandler: (latLng: LatLng?) -> Unit) {
    private var fusedLocationClient: FusedLocationProviderClient
    private val listener: DialogInterface.OnClickListener

    init {
        this.listener = getLocationRequestListener()
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(activity)
    }

    private fun getLocationRequestListener(): DialogInterface.OnClickListener {
        return DialogInterface.OnClickListener { dialog, which ->

            val btnNegative = -2
            val btnPositive = -1

            when (which) {
                btnPositive -> {
                    fragment.requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        PERMISSIONS_REQUEST_LOCATION_VENUE)
                    dialog.dismiss()
                }
                btnNegative -> {
                    dialog.dismiss()
                }
            }
        }
    }

    fun checkLocationPermissionAndGetLocation() {
        if (ContextCompat.checkSelfPermission(activity,
                Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            // Should we show an explanation?
            if (fragment.shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                showMessageOKCancel("We would like your location to show you venues")
            } else {
                // No explanation needed, we can request the permission.
                fragment.requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    PERMISSIONS_REQUEST_LOCATION_VENUE)
            }
        } else {
            // Permission has already been granted
            getLocation()
        }
    }

    private fun showMessageOKCancel(message: String) {
        AlertDialog.Builder(activity)
            .setMessage(message)
            .setPositiveButton("OK", listener)
            .setNegativeButton("Cancel", listener)
            .create()
            .show()
    }

    fun getLocation() {
        if (ContextCompat.checkSelfPermission(activity,
                Manifest.permission.ACCESS_FINE_LOCATION)
            == PackageManager.PERMISSION_GRANTED) {
            fusedLocationClient.lastLocation
                .addOnSuccessListener { location: Location? ->
                    location?.let {
                        val currentLatLng = LatLng(it.latitude, it.longitude)
                        locationReceivedHandler(currentLatLng)
                        Log.i("LOCATION", "We have location - not nil")
                    }

                    if (location == null) {
                        locationReceivedHandler(sydneyLocation)
                        Log.i("LOCATION", "Location is nil")
                    }

                }.addOnFailureListener {
                    Log.i("LOCATION", "Get location failed")
                }.addOnCanceledListener {
                    Log.i("LOCATION", "Get location cancelled")
                }
        } else {
            Log.i("LOCATION", "Permission not granted for location.")
        }
    }
}