package com.pinkzebra.batboardreader

import android.os.Bundle

import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import kotlinx.android.synthetic.main.activity_main.*
import androidx.navigation.fragment.NavHostFragment
import com.google.firebase.ml.common.modeldownload.FirebaseLocalModel
import com.google.firebase.ml.common.modeldownload.FirebaseModelManager
import com.pinkzebra.batboardreader.models.CameraAndDisplayBoardSharedViewModel
import com.pinkzebra.batboardreader.models.FirestoreRepository
import com.pinkzebra.batboardreader.models.Prefs
import com.pinkzebra.batboardreader.views.CustomMenuToolbar


// This is an arbitrary number we are using to keep tab of the permission
// request. Where an app has multiple context for requesting permission,
// this can help differentiate the different contexts

class MainActivity : AppCompatActivity() {
    // private lateinit var model: CameraAndDisplayBoardSharedViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val model = ViewModelProviders.of(this).get(CameraAndDisplayBoardSharedViewModel::class.java)
        model.newBoardData.postValue(null)

        val host: NavHostFragment = nav_host_fragment as NavHostFragment? ?: return
        val navController = host.navController

        val localModel = FirebaseLocalModel.Builder("model")
                .setAssetFilePath("automl/manifest.json")
                .build()
        FirebaseModelManager.getInstance().registerLocalModel(localModel)

        navController.addOnDestinationChangedListener { navController: NavController, navDestination: NavDestination, bundle: Bundle? ->

            val isloggedIn = Prefs(this).isLoggedIn()

            if (isloggedIn == true) {

            } else {
                if (navController.currentDestination?.label != getString(R.string.account_dest)) {
                    customNavigateToTopTab(CustomMenuToolbar.MenuItem.ACCOUNT)
                }
            }

            setNavigationClickabilityBasedOnLogin(isloggedIn)

            when (navDestination.label) {
                getString(R.string.board_capture) -> {
                    // toolbar.visibility = View.GONE
                }
                else -> {
                    //   toolbar.visibility = View.VISIBLE
                }
            }


        }

        getAllNavButtons().forEach {
            it.setOnClickListener {

                CustomMenuToolbar.currentMenuItem = when (it) {
                    outlets_toolbar_btn -> {
                        CustomMenuToolbar.MenuItem.OUTLET
                    }
                    account_toolbar_icon -> {
                        CustomMenuToolbar.MenuItem.ACCOUNT
                    }
                    products_toolbar_btn -> {
                        CustomMenuToolbar.MenuItem.PRODUCTS
                    }
                    reports_toolbar_icon -> {
                        CustomMenuToolbar.MenuItem.REPORTS
                    }
                    else -> {
                        CustomMenuToolbar.MenuItem.OUTLET
                    }
                }
                customNavigateToTopTab(CustomMenuToolbar.currentMenuItem)
            }
        }

        //FirestoreRepository().uploadAllProductJSONDataToFirestore(this)
       // FirestoreRepository().uploadAllVenueJSONDataToFirestore(this)
      //  FirestoreRepository().fetchVenuesRecentPricesMatchingProductIDAndTeamVenues(187)
      //  FirestoreRepository().fetchVenueBoardData(87088)
    }

    fun customNavigateToTopTab(navMenuItem: CustomMenuToolbar.MenuItem) {
        val host: NavHostFragment = nav_host_fragment as NavHostFragment? ?: return
        val navController = host.navController
        when (navMenuItem) {
            CustomMenuToolbar.MenuItem.ACCOUNT -> {
                clearAllTopMenuButtonBackgroundColors()
                account_toolbar_icon.setBackgroundColor(ContextCompat.getColor(this, R.color.cyan))
                navController.navigate(R.id.account_dest)
            }
            CustomMenuToolbar.MenuItem.OUTLET -> {
                clearAllTopMenuButtonBackgroundColors()
                outlets_toolbar_btn.setBackgroundColor(ContextCompat.getColor(this, R.color.cyan))
                navController.popBackStack(R.id.displayBoardResult_dest, false)
            }
            CustomMenuToolbar.MenuItem.REPORTS -> {
                clearAllTopMenuButtonBackgroundColors()
                reports_toolbar_icon.setBackgroundColor(ContextCompat.getColor(this, R.color.cyan))
            }
            CustomMenuToolbar.MenuItem.PRODUCTS -> {
                clearAllTopMenuButtonBackgroundColors()
                products_toolbar_btn.setBackgroundColor(ContextCompat.getColor(this, R.color.cyan))
                navController.navigate(R.id.venueMappedPricing_dest)
            }
        }
    }

    fun setNavigationClickabilityBasedOnLogin(loggedIn: Boolean) {
        if (loggedIn) {
            setAllNavButtonClickability(true)
        } else {
            setAllNavButtonClickability(false)
        }
    }

    private fun getAllNavButtons(): List<View> {
        return listOf(outlets_toolbar_btn, account_toolbar_icon, products_toolbar_btn, reports_toolbar_icon)
    }

    private fun setNavButtonClickability(view: View, enabled: Boolean) {
        view.isEnabled = enabled
    }

    private fun setAllNavButtonClickability(enabled: Boolean) {
        getAllNavButtons().forEach { it.isEnabled = enabled }
    }

    private fun clearAllTopMenuButtonBackgroundColors() {
        outlets_toolbar_btn.setBackgroundColor(ContextCompat.getColor(this, R.color.transparent))
        account_toolbar_icon.setBackgroundColor(ContextCompat.getColor(this, R.color.transparent))
        products_toolbar_btn.setBackgroundColor(ContextCompat.getColor(this, R.color.transparent))
        reports_toolbar_icon.setBackgroundColor(ContextCompat.getColor(this, R.color.transparent))
    }

    override fun onBackPressed() {
        Log.i("MainActiv", "Back button pressed")
        val host: NavHostFragment = nav_host_fragment as NavHostFragment? ?: return
        val navController = host.navController
        if (navController.currentDestination?.label == getString(R.string.board_result)) {
            finish()
        } else {
            navController.popBackStack()
        }
    }
}



