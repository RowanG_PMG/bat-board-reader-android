package com.pinkzebra.batboardreader.fragments

import android.content.ContentValues.TAG
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.graphics.Rect
import android.os.Bundle
import android.os.Environment
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.firestore.FirebaseFirestore
import com.google.gson.Gson
import com.pinkzebra.batboardreader.LocationRequests
import com.pinkzebra.batboardreader.MainActivity
import com.pinkzebra.batboardreader.PERMISSIONS_REQUEST_LOCATION_VENUE
import com.pinkzebra.batboardreader.R
import com.pinkzebra.batboardreader.adapters.*
import com.pinkzebra.batboardreader.models.*
import com.pinkzebra.batboardreader.utils.*
import com.pinkzebra.batboardreader.views.roundedRectWithCornerDrawable
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_display_board_result.*
import kotlinx.android.synthetic.main.fragment_display_board_result.search_text
import kotlinx.android.synthetic.main.fragment_display_board_result.view.*
import kotlinx.android.synthetic.main.image_with_text_horizontal_btn_layout.view.*
import org.jetbrains.anko.toast
import java.io.File
import java.io.FileWriter
import java.io.IOException

const val NEW_BOARD_DATA = "new_board_data"

class DisplayBoardSelectVenueFragment : Fragment(), BoardItemTextListener, VenueListItemListener {

    private var boardCaptureDataHistory: BoardCaptureDataHistory? = null

    private var displayBoardTextItemData: MutableList<BoardTextItemModel> = mutableListOf()
    private var newBoardData: SingleBoardCaptureData? = null
        set(value: SingleBoardCaptureData?) {
            field = value

        }

    private var currentBoardIndex: Int = -1
    private lateinit var boardItemsViewAdapter: BoardItemTextAdapter

    private var venueListData: MutableList<VenueListModel> = mutableListOf()
    private lateinit var venuesViewAdapter: RecyclerView.Adapter<*>
    private var venueListDataToDisplay: MutableList<VenueListModel> = mutableListOf()

    private var productData: List<Product>? = null

    private var locationRequest: LocationRequests? = null
    private var lastLocation: LatLng? = null

    private var currentDisplayedVenueID: Int? = null

    private var unmappedProducts: MutableList<String> = mutableListOf()

    private lateinit var model: CameraAndDisplayBoardSharedViewModel

    private lateinit var boardTypeAdapter: ArrayAdapter<String>

    data class AllAsyncDataStatus(
            val venueDataReady: Boolean = false,
            val locationReady: Boolean = false,
            val productDataReady: Boolean = false
    )

    private var allAsyncDataStatus = AllAsyncDataStatus()
        set(value) {
            field = value
            updateVenuesTableSortByDistanceIfVenueDataAndLocationReady()
            populateBoardDataIfDataReady()

            if (value.locationReady && value.venueDataReady && value.productDataReady) {
                activity!!.pBar.visibility = View.GONE
                showBoardDisplayMode()
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        boardTypeAdapter = ArrayAdapter<String>(context!!, android.R.layout.simple_spinner_dropdown_item, boardTypeItems)

        arguments?.let {
            //   boardCaptureDataHistory = it.getSerializable(VENUE_BOARD_HISTORY) as? BoardCaptureDataHistory
            // newBoardData = it.getSerializable(NEW_BOARD_DATA) as? SingleBoardCaptureData
            //  newBoardFirebaseBoardItemGroups = it.getSerializable(NEW_BOARD_FIREBASE_LINES) as? List<List<FirebaseVisionText.Line>>
        }

        locationRequest = LocationRequests(activity = activity as MainActivity, fragment = this) {
            it?.let { latLng ->
                setCurrentLocation(latLng)
                Log.i("Map", latLng.toString())
            } ?: run {
                Log.i("Map", "Location not found")
            }
        }

        locationRequest?.checkLocationPermissionAndGetLocation()

        model = activity?.run {
            ViewModelProviders.of(this).get(CameraAndDisplayBoardSharedViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        model.newBoardData.observe(this, Observer<SingleBoardCaptureData?> {
            //show_image_btn.isEnabled = false
            newBoardData = it
            Log.i("VM", "View Model called with value $it")
        })


    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_display_board_result, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        showEmptyScreen()
        activity!!.pBar.visibility = View.VISIBLE

        spinner_select_board_type_in_display.setAdapter(boardTypeAdapter)

        spinner_select_board_type_in_display.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val spinnerString =
                        spinner_select_board_type_in_display?.selectedItem.toString()
                newBoardData?.let { newBoardDataNonNull ->
                    newBoardDataNonNull
                    reprocessNewBoardWithSelectedBoardType(newBoardData = newBoardDataNonNull, spinnerString = spinnerString, reprocessNewBoardCompletion = { reprocessedBoardData ->
                        reprocessBoardTypeCompletion.invoke(reprocessedBoardData)
                    })
                }
            }
        }

        venuesViewAdapter = VenueListAdapter(venueListDataToDisplay, this)
        venue_items_recycler_in_display.apply {
            // use this setting to improve performance if you know that changes
            // in captureMechanism do not change the layout size of the RecyclerView
            setHasFixedSize(false)
            layoutManager = LinearLayoutManager(context)
            adapter = venuesViewAdapter
            this.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        }

        val teamID = Prefs(context!!).getLoginID()

        teamID?.let {
            FirestoreRepository().fetchVenueData(teamID = it, completionHandler = {

                val venueListData = it.mapIndexed { index, item ->
                    VenueListModel(
                            venueID = item.venueID,
                            markerID = index,
                            venueName = item.venueName,
                            address = item.address,
                            latLng = item.latLng,
                            isFavorite = false,
                            boardType = "",
                            productPrice = 0f,
                            venueDistance = 0f
                    )

                }
                this.setVenueList(venueListData)
            },
                    failHandler = {
                        Log.i("VenueData", "Venue data fetch failed.")
                    })
        }

        FirestoreRepository().fetchProductData(completionHandler = {
            this.productData = it
            Log.i("Data", "Products: " + it.toString())
            allAsyncDataStatus = allAsyncDataStatus.copy(productDataReady = true)

        })


        next_board.setOnClickListener {
            boardCaptureDataHistory?.itemsHistory?.let { boardCaptureDataArray ->
                if (currentBoardIndex < (boardCaptureDataArray.count() - 1)) {
                    currentBoardIndex += 1
                    makeAndSetBoardDataAndUpdateUI(currentBoardIndex, true)
                }
            }
        }

        previous_board.setOnClickListener {
            boardCaptureDataHistory?.itemsHistory?.let {
                if (currentBoardIndex >= 1) {
                    currentBoardIndex -= 1
                    makeAndSetBoardDataAndUpdateUI(currentBoardIndex, true)
                }
            }
        }

        show_list_btn.btn_image.setImageResource(R.drawable.list_icon)
        show_image_btn.btn_image.setImageResource(R.drawable.board_image_icon)
        show_list_btn.btn_text.text = "List"
        show_image_btn.btn_text.text = "Photo"

        show_list_btn.setOnClickListener {
            board_text_recycler.visibility = View.VISIBLE
            board_image_preview.visibility = View.GONE
        }

        show_image_btn.setOnClickListener {
            board_text_recycler.visibility = View.GONE
            board_image_preview.visibility = View.VISIBLE
            updateBoardImage(currentBoardIndex)
        }

//        updateBoardIndexIndicatorLabel()
//        updateNextPreviousBtnClickabilityFeedback()

//        previous_board.visibility = View.GONE
//        next_board.visibility = View.GONE
        board_index_indicator.visibility = View.GONE

        submit.setOnClickListener {
            newBoardData?.let {
                //                Prefs(context!!).addNewBoardToHistory(
//                        board = it
//                )

                addNewBoardToFirebase(it, completionHandler = {

                    submit_cancel_new_board.visibility = View.GONE
                    newBoardData = null
                    updateNextPreviousBtnClickabilityFeedback(currentBoardIndex)
                })
            }

        }

        cancel.setOnClickListener {
            newBoardData = null
            showVenuesDisplayMode()
        }

        //  review_btn.background = roundedRectWithCornerDrawable(context!!, R.color.cyan, 1, R.color.black, 10f)
        show_list_btn.background = roundedRectWithCornerDrawable(context!!, R.color.cyan, 1, R.color.black, 5f)
        show_image_btn.background = roundedRectWithCornerDrawable(context!!, R.color.cyan, 1, R.color.black, 5f)

        cancel.background = roundedRectWithCornerDrawable(context!!, R.color.cyan, 1, R.color.black, 5f)
        submit.background = roundedRectWithCornerDrawable(context!!, R.color.cyan, 1, R.color.black, 5f)

        search_text.setOnEditorActionListener { v, actionId, event ->

            if (event?.getKeyCode() === KeyEvent.KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_SEARCH) {
                Log.i(TAG, "Enter pressed")
                v.clearFocus()
                showBoardDisplayMode()
                this@DisplayBoardSelectVenueFragment.view?.hideKeyboard()
                true
            } else {
                Log.i("Data", "Some other action event")
                false
            }
        }

        search_text.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                Log.i("Data", "Search text changed")
                showVenuesDisplayMode()

                venueListDataToDisplay.clear()
                val newFilteredVenues = venueListData.filter {
                    it.venueName.toLowerCase().contains(search_text.text.toString().toLowerCase())
                }
                venueListDataToDisplay.addAll(newFilteredVenues)
                venuesViewAdapter.notifyDataSetChanged()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        search_text.setOnFocusChangeListener { view, hasFocus ->
            if (hasFocus) {
                //   activity!!.toast("focused")
                showVenuesDisplayMode()
            } else {
                //  activity!!.toast("focus lost")
            }
        }

        camera_selection_holder.setOnClickListener {
            cameraIconClicked()
        }


    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSIONS_REQUEST_LOCATION_VENUE -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    // permission was granted
                    locationRequest?.getLocation()
                } else {
                    // permission denied
                    Log.i("Location", "Location not called. Result lacks necessary info.")
                    val sydneyLocation = LatLng(-33.8688, 151.2093)
                    setCurrentLocation(sydneyLocation)
                }
                return
            }

            else -> {
                // Ignore all other requests.
                Log.i("Location", "Other permission result received.")
            }
        }
    }

    private fun showBoardDisplayMode() {
        board_display_group.visibility = View.VISIBLE
        venue_items_recycler_in_display.visibility = View.GONE
    }

    private fun showVenuesDisplayMode() {
        board_display_group.visibility = View.GONE
        venue_items_recycler_in_display.visibility = View.VISIBLE
    }

    private fun showEmptyScreen() {
        board_display_group.visibility = View.GONE
        venue_items_recycler_in_display.visibility = View.GONE
        spinner_select_board_type_in_display.visibility = View.GONE
        previous_next_date_area.visibility = View.GONE
        timestamp_price_board.visibility = View.GONE
        submit_cancel_new_board.visibility = View.GONE
        show_list_image_compliance_area.visibility = View.GONE
    }

    private fun cameraIconClicked() {
        currentDisplayedVenueID?.let {
            val currentDisplayedVenue = getVenueByVenueID(it)
            currentDisplayedVenue?.let {
                val bundle = Bundle()
                bundle.putSerializable(VENUE_NAME, it.venueName)
                bundle.putSerializable(VENUE_ID, it.venueID)
                findNavController().navigate(R.id.captureImage_dest, bundle)
            }
        }
    }

    private fun populateBoardDataIfDataReady() {
        if (allAsyncDataStatus.productDataReady == true && allAsyncDataStatus.venueDataReady == true && allAsyncDataStatus.locationReady == true) {
            productData?.let {
                boardItemsViewAdapter = BoardItemTextAdapter(displayBoardTextItemData, productData = it, listener = this)

                newBoardData?.let { newBoardData ->
                    val venueName = newBoardData.venueName
                    val venueID = newBoardData.venueID

//            val lastStoredBoardData = Prefs(context!!).getBoardHistory()
//            val productDataThisVenue =
//                lastStoredBoardData?.itemsHistory?.filter { it.venueName == venueName }?.toMutableList()
//                    ?: mutableListOf()
//            productDataThisVenue.add(newBoardData)

                    venueID?.let {
                        val venue = getVenueByVenueID(it)
                        venue?.let {
                            displayVenueBasicData(it)
                        }

                        fetchBoardDataForVenueAndUpdateBoardDisplay(venueID)
                    }

                    var boardTypeDetected: String
                    if (newBoardData.boardType != "") {
                        boardTypeDetected = newBoardData.boardType
                    } else {
                        boardTypeDetected = "None"
                    }

                    //  activity!!.toast("Board type detected: ${boardTypeDetected}")
                    setBoardTypeSpinnerSelectedValue(boardTypeDetected)
                } ?: run {
                    val closestVenue = venueListDataToDisplay.firstOrNull()
                    closestVenue?.let {
                        val venueID = it.venueID
                        displayVenueBasicData(it)
                        fetchBoardDataForVenueAndUpdateBoardDisplay(venueID)
                    } ?: run {
                        showEmptyScreen()
                        activity?.toast("We encountered a problem. Check internet.")
                    }
                }
//            Snackbar.make(
//                activity!!.findViewById(android.R.id.content),
//                "Board type detected: ${boardTypeDetected}",
//                Snackbar.LENGTH_LONG
//            ).show()
                board_text_recycler.apply {
                    // use this setting to improve performance if you know that changes
                    // in captureMechanism do not change the layout size of the RecyclerView
                    setHasFixedSize(false)
                    layoutManager = LinearLayoutManager(context)
                    adapter = boardItemsViewAdapter
                }
            }
        }
    }

    fun getVenueByVenueID(venueID: Int): VenueListModel? {
        val venue = venueListData.find { it.venueID == venueID }
        return venue
    }

    private fun displayVenueBasicData(venue: VenueListModel) {
        venue_name_price_board.text = venue.venueName
        venue_address_price_board.text = venue.address

        currentDisplayedVenueID = venue.venueID
    }

    private fun fetchBoardDataForVenueAndUpdateBoardDisplay(venueID: Int) {
        activity?.pBar?.visibility = View.VISIBLE
        clearBoardImage()
        clearBoardItemText()
        clearOCROverlay()
        FirestoreRepository().fetchBoardHistoryFirebase(venueID = venueID, completionHandler = {
            activity?.pBar?.visibility = View.GONE
            val mutableBoardHistory = it.toMutableList()
            newBoardData?.let {
                val mappedProducts = it.copy(itemData = mapAllDetectedProducts(it.itemData).toMutableList())
                val complianceMappedProducts = mappedProducts.copy(itemData = mapAllLineItemsForCompliance(mappedProducts.itemData).toMutableList())
                newBoardData = complianceMappedProducts
                writeUnmappedNamesToFile(unmappedProducts)
                mutableBoardHistory.add(complianceMappedProducts)
            }
            boardCaptureDataHistory = BoardCaptureDataHistory(itemsHistory = mutableBoardHistory)
            currentBoardIndex = mutableBoardHistory.count() - 1
            if (currentBoardIndex >= 0) {
                makeAndSetBoardDataAndUpdateUI(currentBoardIndex)
            } else {
                updateEmptyDataAndListImageComplianceDisplayUI()
                updateCancelSubmitArea()
                hidePreviousNextTimeAndBoardIndexIndicator()
                updateDropDownBoardTypeVisibility()
            }
        }, failHandler = {

        })
    }

    private fun doProductMappingSingleProduct(productPrice: ProductPrice): ProductPrice? {
        val mappedProductNameWithSize = convertProductPriceToMappedProductWithSize(productPrice)
        val mappedProductNameWithSizeAndReshapeMixupCharacters = mappedProductNameWithSize.replace("0", "O")
        val reshapedProductNameUpperCased = mappedProductNameWithSizeAndReshapeMixupCharacters.toUpperCase()
        productData?.let {
            val lookedUpProduct = it.firstOrNull {
                val reshapedLookupsOnMixupCharacters = it.lookup?.map { it.replace("0", "O") }
                        ?: listOf()
                val reshapedUpperCase = reshapedLookupsOnMixupCharacters.map { it.toUpperCase() }

                reshapedUpperCase.contains(reshapedProductNameUpperCased)
            }
            if (lookedUpProduct != null) {
                return productPrice.copy(productName = lookedUpProduct.name, wasAutoMapped = true, manufacturerID = lookedUpProduct.manufacturerID, productID = lookedUpProduct.productID)
            } else {
                unmappedProducts.add(mappedProductNameWithSize)
                Log.i("Mapping", "Product Mapping to add: $mappedProductNameWithSize")
                print(mappedProductNameWithSize + ",")
                return null
            }
        } ?: return null
    }

    private fun getProductFromID(productID: Int): Product? {
        productData?.let {
            val foundProduct = it.find { it.productID == productID }
            return foundProduct
        }
        return null
    }

    private fun mapAllDetectedProducts(items: List<ProductPrice>): List<ProductPrice> {
        return items.map {
            doProductMappingSingleProduct(it) ?: it.copy(wasAutoMapped = false)
        }
    }

    private fun writeUnmappedNamesToFile(list: List<String>) {
        var csvList: String = ""
        for (item in list) {
            csvList += item + ","
        }
        Log.i("Mapping", csvList)
        var fileWriter: FileWriter? = null

        try {
            fileWriter = FileWriter(File(activity!!.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS), "unmapped_products.csv"))

            fileWriter.append("UnmappedProducts")
            fileWriter.append('\n')

            for (product in list) {
                fileWriter.append(product)
                fileWriter.append('\n')
            }
            println("Write CSV successfully!")
        } catch (e: Exception) {
            println("Writing CSV error!")
            e.printStackTrace()
        } finally {
            try {
                fileWriter!!.flush()
                fileWriter.close()
            } catch (e: IOException) {
                println("Flushing/closing error!")
                e.printStackTrace()
            }
        }
    }

    private fun convertProductPriceToMappedProductWithSize(productPrice: ProductPrice): String {
        var productName = productPrice.productName.replace(" ", "").toUpperCase()
        productName = replaceWhiteSpaceWith(productName, "")

        return "${productName}${productPrice.productSize}"
    }

    override fun listItemClicked(position: Int) {
        val venue = venueListDataToDisplay[position]
        val venueID = venue.venueID
        newBoardData = null
        showBoardDisplayMode()
        displayVenueBasicData(venue)
        fetchBoardDataForVenueAndUpdateBoardDisplay(venueID)
        this.view?.hideKeyboard()
        search_text.clearFocus()
    }

    override fun cameraCaptureClicked(position: Int) {

    }

    private fun updateVenuesTableSortByDistanceIfVenueDataAndLocationReady() {
        if (allAsyncDataStatus.locationReady && allAsyncDataStatus.venueDataReady) {
            venueListData = venueListData.map {
                val venueDistance = distanceBetweenPoints(lastLocation!!, it.latLng).toFloat()
                val venueDistanceTrimmedDecimals = (Math.round(venueDistance * 100) / 100.0).toFloat()
                it.copy(venueDistance = venueDistanceTrimmedDecimals)
            }.toMutableList()

            updateVenueTableSortedByDistance()
        }
    }

    private fun updateVenueTableSortedByDistance() {
        venueListDataToDisplay.clear()

        val sortedByNearest = venueListData.sortedBy {
            it.venueDistance
        }

        venueListDataToDisplay.addAll(sortedByNearest)
        venuesViewAdapter.notifyDataSetChanged()
    }

    private fun setVenueList(list: List<VenueListModel>) {
        this.venueListData.clear()
        this.venueListData.addAll(list)
        Log.i("Data", list.toString())
        allAsyncDataStatus = allAsyncDataStatus.copy(venueDataReady = true)
    }

    private fun setCurrentLocation(latLng: LatLng) {
        lastLocation = latLng
        Log.i("Data", "Location: " + lastLocation.toString())
        allAsyncDataStatus = allAsyncDataStatus.copy(locationReady = true)
    }

    override fun dropdownItemSelected(productID: Int?, itemIndex: Int) {
        Log.i("Product", "Dropdown Item selected, product ID: $productID at index: $itemIndex")

        newBoardData?.let {
            var latestBoardData = boardCaptureDataHistory?.itemsHistory?.last()

            latestBoardData?.let {
                val item = it.itemData[itemIndex]
                var matchingProduct: Product? = null
                productID?.let {
                    matchingProduct = getProductFromID(productID)
                }
                matchingProduct?.let {
                    item.productID = productID
                    item.productSize = it.size
                    item.productName = it.name
                    item.genericPackageType = it.packageType
                    item.manufacturerID = it.manufacturerID
                } ?: run { item.productID = null }
                boardCaptureDataHistory?.itemsHistory?.lastIndex?.let { lastIndex ->
                    boardCaptureDataHistory?.itemsHistory?.last()?.let {
                        it.itemData[itemIndex] = item
                        it.itemData = mapAllLineItemsForCompliance(it.itemData).toMutableList()
                        newBoardData = it
                    }
                    makeAndSetBoardDataAndUpdateUI(lastIndex)
                }
            }
        }
    }

    private fun allUnmappedProductsSelected(boardItems: List<ProductPrice>): Boolean {
        for (item in boardItems) {
            if (item.productID == null) {
                return false
            }
        }
        return true
    }

    private fun addNewBoardToFirebase(item: SingleBoardCaptureData, completionHandler: (() -> Unit)? = null) {
        val db = FirebaseFirestore.getInstance()
        val gson = Gson()

        val uniqueImageIdentifier = "${System.currentTimeMillis()}.jpg"

        val boardDataWithRemoteFilename = item.copy(boardImageRemoteFilename = uniqueImageIdentifier)

        val json = gson.toJson(boardDataWithRemoteFilename)

        val board_history_item = hashMapOf(
                "json_data" to json,
                "venueID" to item.venueID,
                "local_board_image" to item.boardImageUriString,
                "board_image" to uniqueImageIdentifier,
                "timestamp" to item.timestamp,
                "boardType" to item.boardType,
                "itemData" to item.itemData
        )

        val numberAsyncOperations = 2
        var numberAsyncOperationsCompleted = 0

        activity?.pBar?.visibility = View.VISIBLE

        db.collection("board_data")
                .add(board_history_item)
                .addOnSuccessListener { documentReference ->
                    Log.d(TAG, "DocumentSnapshot added with ID: ${documentReference.id}")
                }
                .addOnFailureListener { e ->
                    Log.w(TAG, "Error adding document", e)
                }.addOnCompleteListener {
                    numberAsyncOperationsCompleted++
                    handleBoardDataMultiPartUpload(
                            numberAsyncOperationsCompleted,
                            numberAsyncOperations,
                            completionHandler = { completionHandler?.invoke() })
                }

        val imageUrlString = item.boardImageUriString
        val uriImage = getUriFromString(imageUrlString)

        uriImage?.let {
            FirestoreRepository().uploadImageToFirestore(
                    imageUri = it,
                    filename = uniqueImageIdentifier,
                    completionHandler = {
                        numberAsyncOperationsCompleted++
                        handleBoardDataMultiPartUpload(
                                numberAsyncOperationsCompleted,
                                numberAsyncOperations,
                                completionHandler = { completionHandler?.invoke() })
                    })
        }

    }

    private fun makeAndSetBoardDataAndUpdateUI(boardIndex: Int, refreshBoardTableUI: Boolean = false) {
        boardItemsViewAdapter.boardEditable = newBoardData != null
        val thisBoard = boardCaptureDataHistory?.itemsHistory?.let {
            it[boardIndex]
        }

        val thisBoardType = thisBoard?.boardType

        val boardTextDataNextToShow = makeBoardTextData(boardIndex)
        boardTextDataNextToShow?.let {

            showTimeAndPreviousNext()
            setBoardDisplayData(it)
            updateEmptyDataAndListImageComplianceDisplayUI()
            if (refreshBoardTableUI) {
                boardItemsViewAdapter.notifyDataSetChanged()
            }
            updateBoardImage(boardIndex)
            updateTimestampLabel(boardIndex)
            updateBoardIndexIndicatorLabel(boardIndex)
            updateNextPreviousBtnClickabilityFeedback(boardIndex)
            updateComplianceFeedback(it, thisBoardType)
            updateDropDownBoardTypeVisibility()

        } ?: run {
            hidePreviousNextTimeAndBoardIndexIndicator()
        }
        updateCancelSubmitArea()
    }

    private fun showTimeAndPreviousNext() {
        previous_next_date_area.visibility = View.VISIBLE
        timestamp_price_board.visibility = View.VISIBLE
    }

    private fun hidePreviousNextTimeAndBoardIndexIndicator() {
        previous_next_date_area.visibility = View.GONE
        timestamp_price_board.visibility = View.GONE
        board_index_indicator.visibility = View.GONE
    }

    private fun updateDropDownBoardTypeVisibility() {
        newBoardData?.let {
            spinner_select_board_type_in_display.visibility = View.VISIBLE
        } ?: run {
            spinner_select_board_type_in_display.visibility = View.GONE
        }
    }

    private fun updateSubmitButtonClickability() {
        newBoardData?.let {
            val items = it.itemData
            if (allUnmappedProductsSelected(items)) {
                submit.isEnabled = true
                setButtonDisabledColor(submit, ContextCompat.getColor(context!!, R.color.cyan))
            } else {
                submit.isEnabled = false

                setButtonDisabledColor(submit, ContextCompat.getColor(context!!, R.color.light_gray))
            }
        }
    }

    private fun reprocessNewBoardWithSelectedBoardType(newBoardData: SingleBoardCaptureData, spinnerString: String, reprocessNewBoardCompletion: (SingleBoardCaptureData) -> Unit) {
        BoardReaderAlgorithm(activity = activity as MainActivity).doAutoBoardDetectIfNeededThenProcessOCR(spinnerString = spinnerString, context = context!!, singleBoardCaptureData = newBoardData, algoCompletion = { reprocessedBoardData ->
            reprocessBoardTypeCompletion.invoke(reprocessedBoardData)
        })
    }

    private val reprocessBoardTypeCompletion: (SingleBoardCaptureData) -> Unit =
            { reprocessedBoardData ->
                val mappedProducts = reprocessedBoardData.copy(itemData = mapAllDetectedProducts(reprocessedBoardData.itemData).toMutableList())
                val complianceMappedProducts = mappedProducts.copy(itemData = mapAllLineItemsForCompliance(mappedProducts.itemData).toMutableList())
                this.newBoardData = complianceMappedProducts

                var itemsHistory = boardCaptureDataHistory?.itemsHistory
                itemsHistory?.let {
                    val boardIndexNewBoard = it.count() - 1
                    updateSingleBoardCaptureDataHistoryAtBoardIndex(boardIndexNewBoard, complianceMappedProducts)
                    makeAndSetBoardDataAndUpdateUI(boardIndexNewBoard)
                    it.last()?.let {
                        setBoardTypeSpinnerSelectedValue(it.boardType)
                    }
                }
            }

    private fun updateSingleBoardCaptureDataHistoryAtBoardIndex(index: Int, newSingleBoardCaptureData: SingleBoardCaptureData) {
        var itemsHistory = boardCaptureDataHistory?.itemsHistory
        itemsHistory?.let {
            if (index <= itemsHistory.count() - 1) {
                it[index] = newSingleBoardCaptureData
            }
        }
    }

    private fun handleSpinnerMultipleFirebaseUploadQueue(
            numberAsyncOperationsCompleted: Int,
            numberAsyncOperationsNeeded: Int
    ) {
        if (numberAsyncOperationsCompleted >= numberAsyncOperationsNeeded) {
            activity?.pBar?.visibility = View.GONE
        } else {
            activity?.pBar?.visibility = View.VISIBLE
        }
    }

    private fun handleBoardDataMultiPartUpload(
            numberAsyncOperationsCompleted: Int,
            numberAsyncOperationsNeeded: Int,
            completionHandler: (() -> Unit)? = null
    ) {
        handleSpinnerMultipleFirebaseUploadQueue(numberAsyncOperationsCompleted, numberAsyncOperationsNeeded)

        if (numberAsyncOperationsCompleted >= numberAsyncOperationsNeeded) {
            completionHandler?.invoke()
        }
    }

    private fun updateComplianceFeedback(boardItems: List<BoardTextItemModel>, boardType: String?) {
        var allComplianceChecksPassed: Boolean = true

        if (areAllBoardItemsCompliant(boardItems, boardType)) {
            compliance_icon.visibility = View.GONE
            compliance_overall_text.visibility = View.GONE
        } else {
            compliance_icon.visibility = View.VISIBLE
            compliance_overall_text.visibility = View.VISIBLE
        }
    }

    private fun areAllBoardItemsCompliant(boardItems: List<BoardTextItemModel>, boardType: String?): Boolean {
        var counter = 0
        for (item in boardItems) {
            if (item.packageType == PACK) {
                counter += 1
                item.manufacturerID?.let {
                    if (passesCompliance(item.manufacturerID, counter)) {
                    } else {
                        return false
                    }
                }
            }
        }
        return true
    }

    private fun mapAllLineItemsForCompliance(items: List<ProductPrice>): List<ProductPrice> {
        var counter = 0
        return items.map { productPrice ->
            if (productPrice.packageType == PACK) {
                counter += 1
                productPrice.manufacturerID?.let {
                    if (passesCompliance(it, counter)) {
                        return@map productPrice.copy(isCompliant = true)
                    } else {
                        return@map productPrice.copy(isCompliant = false)
                    }
                }
            }
            return@map productPrice
        }
    }

    private fun updateBoardImage(boardHistoryIndex: Int) {
        boardCaptureDataHistory?.itemsHistory?.let {
            val singleBoardItem = it[boardHistoryIndex]

            val firebaseImageFilename = singleBoardItem.boardImageRemoteFilename

            clearOCROverlay()

            firebaseImageFilename?.let {
                view?.post {
                    activity?.pBar?.visibility = View.VISIBLE
                    FirestoreRepository().getBoardImageFromFirebase(
                            it,
                            context!!,
                            board_image_preview,
                            { width, height ->
                                activity?.pBar?.visibility = View.GONE
                                if (width != null && height != null) {
                                    withImageShowOverlayBoxes(width, height, singleBoardItem)
                                }
                            })
                }
            } ?: run {
                val uRiImageString = singleBoardItem.boardImageUriString
                val uriImage = getUriFromString(uRiImageString)
                uriImage?.let {
                    view?.post {
                        loadImageFromUriIntoView(uriImage, board_image_preview, successHandler = { width, height ->
                            if (width != null && height != null) {
                                withImageShowOverlayBoxes(width, height, singleBoardItem)
                            }
                        })
                    }
                }
            }
        }
    }

    private fun withImageShowOverlayBoxes(width: Int, height: Int, singleBoardItem: SingleBoardCaptureData) {
        val scaleFactorToGetCenterInside = getScaleFactorForCenterInsideBoardImageFromOCRSize(
                width,
                height,
                board_image_preview.width,
                board_image_preview.height
        )

        val centerInsideScaledBoardImageDimensions = getCenterInsideScaledBoardImageDimensions(
                width,
                height,
                previewImageWidth = board_image_preview.width,
                previewImageHeight = board_image_preview.height
        )

        singleBoardItem.rectData?.let {
            showOverlayBoxesOnBoard(
                    boardLines = singleBoardItem.rectData,
                    centerInsideImageWidth = centerInsideScaledBoardImageDimensions.first,
                    centerInsideImageHeight = centerInsideScaledBoardImageDimensions.second,
                    scaleFactorToGetCenterInside = scaleFactorToGetCenterInside
            )
        } ?: clearOCROverlay()
    }

    private fun clearBoardItemText() {
        displayBoardTextItemData.clear()
        boardItemsViewAdapter.notifyDataSetChanged()
    }

    private fun clearOCROverlay() {
        bounding_box_ocr_text.setBoxes(listOf())
    }

    private fun clearBoardImage() {
        board_image_preview.setImageBitmap(null)
    }

    private fun updateBoardIndexIndicatorLabel(boardIndex: Int) {
        boardCaptureDataHistory?.itemsHistory?.let {
            val numberOfBoards = it.count()
            if (numberOfBoards >= 1) {
                board_index_indicator.text = "${boardIndex + 1} / ${numberOfBoards}"
                board_index_indicator.visibility = View.VISIBLE
                return
            }
        }
        board_index_indicator.visibility = View.GONE
    }

    private fun updateTimestampLabel(boardIndex: Int) {
        boardCaptureDataHistory?.itemsHistory?.let {
            val tZTimestamp = it[boardIndex].timestamp
            timestamp_price_board.text =
                    "Captured: ${convertTimeZoneStampToUserPrintedSydneyDateAndTimeFormat(tZTimestamp)}"
        }
    }

    private fun updateNextPreviousBtnClickabilityFeedback(boardIndex: Int) {
        if (newBoardData == null) {
            previous_next_date_area.visibility = View.VISIBLE
            boardCaptureDataHistory?.itemsHistory?.let {
                val boardCount = it.count()
                if (boardCount >= 1) {
                    if (boardIndex < boardCount - 1) {
                        next_board.btn_image_next.setImageTintList(
                                ColorStateList.valueOf(
                                        ContextCompat.getColor(
                                                context!!,
                                                R.color.royal_navy
                                        )
                                )
                        )
                        next_board.btn_text_next.setTextColor(ContextCompat.getColor(context!!, R.color.royal_navy))
                    } else {
                        next_board.btn_image_next.setImageTintList(
                                ColorStateList.valueOf(
                                        ContextCompat.getColor(
                                                context!!,
                                                R.color.light_gray
                                        )
                                )
                        )
                        next_board.btn_text_next.setTextColor(ContextCompat.getColor(context!!, R.color.light_gray))
                    }

                    if (boardIndex > 0) {
                        previous_board.btn_image_previous.setImageTintList(
                                ColorStateList.valueOf(
                                        ContextCompat.getColor(
                                                context!!,
                                                R.color.royal_navy
                                        )
                                )
                        )
                        previous_board.btn_text_previous.setTextColor(ContextCompat.getColor(context!!, R.color.royal_navy))
                    } else {
                        previous_board.btn_image_previous.setImageTintList(
                                ColorStateList.valueOf(
                                        ContextCompat.getColor(
                                                context!!,
                                                R.color.light_gray
                                        )
                                )
                        )
                        previous_board.btn_text_previous.setTextColor(ContextCompat.getColor(context!!, R.color.light_gray))
                    }
                }
            }
        } else {
            previous_next_date_area.visibility = View.GONE
        }
    }

    private fun updateCancelSubmitArea() {
        updateSubmitButtonClickability()
        if (newBoardData != null) {
            submit_cancel_new_board.visibility = View.VISIBLE
        } else {
            submit_cancel_new_board.visibility = View.GONE
        }

    }

    private fun setBoardDisplayData(data: List<BoardTextItemModel>) {
        displayBoardTextItemData?.clear()
        displayBoardTextItemData?.addAll(data)
    }

    private fun updateEmptyDataAndListImageComplianceDisplayUI() {
        if (displayBoardTextItemData.count() > 0) {
            no_text_label.visibility = View.GONE
            show_list_image_compliance_area.visibility = View.VISIBLE
        } else {
            no_text_label.visibility = View.VISIBLE
            // show_list_btn.isEnabled = false
            //  show_image_btn.isEnabled = false
            show_list_image_compliance_area.visibility = View.GONE
            //  setButtonDisabledColor(show_list_btn, ContextCompat.getColor(context!!, R.color.light_gray))
        }
    }

    private fun setButtonDisabledColor(btn: View, disabledColor: Int) {
        btn.setBackgroundColor(disabledColor)
    }

    private fun makeBoardTextData(boardHistoryIndex: Int): List<BoardTextItemModel>? {

        boardCaptureDataHistory?.itemsHistory?.let {
            val currentBoardDataItemData = it[boardHistoryIndex].itemData

            if (boardHistoryIndex >= 1) {
                val previousBoardData = it[boardHistoryIndex - 1].itemData

                val boardTextDataComparedWithPrevious: List<BoardTextItemModel> =
                        currentBoardDataItemData.map { productItem ->

                            val firstProductMatchIndex =
                                    previousBoardData.indexOfFirst {
                                        productItem.productName.toLowerCase() == it.productName.toLowerCase() && productItem.productSize == it.productSize && it.packageType == productItem.packageType
                                    }

                            if (firstProductMatchIndex != -1) {
                                val lastProductMatchesBoardItem =
                                        previousBoardData[firstProductMatchIndex]
                                val percenDiff = productItem.price - lastProductMatchesBoardItem.price
                                mapProductPriceToBoardItemData(productItem, percenDiff)
                            } else {
                                mapProductPriceToBoardItemData(productItem)
                            }
                        }
                return boardTextDataComparedWithPrevious
            } else {
                return currentBoardDataItemData.map {
                    mapProductPriceToBoardItemData(it)
                }
            }
        } ?: return null
    }

    private fun mapProductPriceToBoardItemData(
            productPrice: ProductPrice,
            percenDiffFromPreviousPrice: Float? = null
    ): BoardTextItemModel {

        return BoardTextItemModel(
                productID = productPrice.productID,
                productName = productPrice.productName,
                productSize = productPrice.productSize,
                productPrice = productPrice.price,
                packageType = productPrice.packageType,
                priceDifference = percenDiffFromPreviousPrice,
                wasAutoMapped = productPrice.wasAutoMapped,
                manufacturerID = productPrice.manufacturerID,
                isCompliant = productPrice.isCompliant
        )
    }

    private fun showOverlayBoxesOnBoard(
            boardLines: List<List<CustomRect?>>,
            centerInsideImageWidth: Int,
            centerInsideImageHeight: Int,
            scaleFactorToGetCenterInside: Float
    ) {
        var allBoundingBoxes = mutableListOf<CustomRect>()

        for (itemLineGroup in boardLines) {
            for (line in itemLineGroup) {
                line?.let {
                    allBoundingBoxes.add(it)
                }
            }
        }

        val allBoundingTransformed = allBoundingBoxes.map {

            val xOffset = (board_image_preview.width - centerInsideImageWidth) / 2
            val yOffset = (board_image_preview.height - centerInsideImageHeight) / 2

            Rect(
                    (it.left / scaleFactorToGetCenterInside).toInt() + xOffset,
                    (it.top / scaleFactorToGetCenterInside).toInt() + yOffset,
                    (it.right / scaleFactorToGetCenterInside).toInt() + xOffset,
                    (it.bottom / scaleFactorToGetCenterInside).toInt() + yOffset
            )
        }

        bounding_box_ocr_text?.let {
            it.setBoxes(allBoundingTransformed)
        }
    }

    private fun getScaleFactorForCenterInsideBoardImageFromOCRSize(
            boardImageOCRWidth: Int,
            boardImageOCRHeight: Int,
            previewImageWidth: Int,
            previewImageHeight: Int
    ): Float {
        val widthRatioOCRAndPreviewFrame = boardImageOCRWidth.toFloat() / previewImageWidth
        val heightRatioOCRAndPreviewFrame = boardImageOCRHeight.toFloat() / previewImageHeight

        val scaleFactorForCenterInside = maxOf(widthRatioOCRAndPreviewFrame, heightRatioOCRAndPreviewFrame)
        return scaleFactorForCenterInside
    }

    private fun getCenterInsideScaledBoardImageDimensions(
            boardImageOCRWidth: Int,
            boardImageOCRHeight: Int,
            previewImageWidth: Int,
            previewImageHeight: Int
    ): Pair<Int, Int> {
        val scaleFactorForCenterInside = getScaleFactorForCenterInsideBoardImageFromOCRSize(
                boardImageOCRWidth,
                boardImageOCRHeight,
                previewImageWidth,
                previewImageHeight
        )

        val widthCenterInsidePreviewContent = (boardImageOCRWidth.toFloat() / scaleFactorForCenterInside).toInt()
        val heightCenterInsidePreviewContent = (boardImageOCRHeight.toFloat() / scaleFactorForCenterInside).toInt()

        return Pair(widthCenterInsidePreviewContent, heightCenterInsidePreviewContent)
    }

    private fun setBoardTypeSpinnerSelectedValue(stringVal: String) {
        val selectionPosition = boardTypeAdapter.getPosition(stringVal)
        spinner_select_board_type_in_display.setSelection(selectionPosition)
    }
}
