package com.pinkzebra.batboardreader.fragments

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.util.Rational
import android.util.Size
import android.util.SparseIntArray
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.camera.core.*
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.pinkzebra.batboardreader.MainActivity
import com.pinkzebra.batboardreader.R
import com.pinkzebra.batboardreader.models.*
import com.pinkzebra.batboardreader.utils.getImageRotation
import com.pinkzebra.batboardreader.utils.makeDestinationBoardImageFile
import com.pinkzebra.batboardreader.views.CustomFontAlertDialog
import com.pinkzebra.batboardreader.views.roundedRectWithCornerDrawable
import com.squareup.picasso.Picasso
import com.yalantis.ucrop.UCrop
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_capture_image.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast
import org.jetbrains.anko.uiThread
import java.io.File

val cameraPermissions = arrayOf(
        Manifest.permission.CAMERA,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE
)
const val PERMISSIONS_REQUEST_CAMERA = 100
const val REQUEST_GALLERY_READ_PERMISSIONS = 2

// This is an array of all the permission specified in the manifest
private val GALLERY_READ_PERMISSIONS = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)
private val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)

private var lastUriFromGallery: Uri? = null
private var lastUriUsedForDetection: Uri? = null
private var lastOriginalImageUri: Uri? = null

const val VENUE_NAME = "venue_name"
const val VENUE_ID = "venue_id"
const val BOARD_TYPE = "board_type"

private const val SCAN_RECEIPT_GALLERY_REQUEST_CODE = 1

val boardTypeItems = listOf<String>(
        AUTO_DETECT,
        BOARD_TYPE_1,
        BOARD_TYPE_2,
        BOARD_TYPE_3,
        BOARD_TYPE_4,
        BOARD_TYPE_5,
        BOARD_TYPE_6
)

class CaptureImageFragment : Fragment(), LifecycleOwner, CustomFontAlertDialog.DialogListener {

    private lateinit var model: CameraAndDisplayBoardSharedViewModel

    private val chooseGalleryTargetAfterSelection = object : com.squareup.picasso.Target {
        override fun onBitmapFailed(e: java.lang.Exception?, errorDrawable: Drawable?) {
            Log.i("PICASSO", "Failed- scan receipt image load")
        }

        override fun onPrepareLoad(placeHolderDrawable: Drawable?) {

        }

        override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
            bitmap?.let { bitmap ->
                bitmap
                (activity as? MainActivity)?.let {
                    LineItemsGroupingAlgorithm(it).processImageFromFile(bitmap, 90f, lastUriUsedForDetection, groupingCompletion = groupingAlgorithmCompletion)
                }

            }
        }
    }

    private val groupingAlgorithmCompletion: (SingleBoardCaptureData)-> Unit =
    {
        val singleBoardDataFromGroupingAlgo = it.copy(venueName = venueName ?: "", venueID = venueID)
        (activity as? MainActivity)?.let {
            val spinnerString =
                    spinner_select_board_type?.selectedItem.toString()

            BoardReaderAlgorithm(activity = it).doAutoBoardDetectIfNeededThenProcessOCR(spinnerString = spinnerString, context = context!!, singleBoardCaptureData = singleBoardDataFromGroupingAlgo, algoCompletion = {
                algorithmCompletion(it)
            })

        }
    }

    private val algorithmCompletion: (SingleBoardCaptureData)-> Unit =
            {
                val singleBoardDataFromGroupingAlgo = it.copy(venueName = venueName ?: "", venueID = venueID)

                sendNewBoardForDisplay(it)
            }

    private val ORIENTATIONS = SparseIntArray()

    init {
        ORIENTATIONS.append(Surface.ROTATION_0, 90)
        ORIENTATIONS.append(Surface.ROTATION_90, 0)
        ORIENTATIONS.append(Surface.ROTATION_180, 270)
        ORIENTATIONS.append(Surface.ROTATION_270, 180)
    }

    private lateinit var viewFinder: TextureView

    var preview: Preview? = null
    var imageCapture: ImageCapture? = null

    val imageCaptureAspectRatioNumerator = 3
    val imageCaptureAspectRatioDenominator = 4

    private var venueName: String? = null
    private var venueID: Int? = null
    private var boardType: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        arguments?.let {
            venueName = it.getString(VENUE_NAME)
            venueID = it.getInt(VENUE_ID)
            boardType = it.getString(BOARD_TYPE)
        }

        model = activity?.run {
            ViewModelProviders.of(this).get(CameraAndDisplayBoardSharedViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    override fun onResume() {
        super.onResume()
        activity!!.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT)

        if (startCameraOnLaunch == true) {
            view_finder.visibility = View.VISIBLE
            startCameraOnLaunch()
        }
        startCameraOnLaunch = true
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_capture_image, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewFinder = view_finder
        // Request camera permissions


        Log.i("Camera", "onviewcreated")

        spinner_board_type_selector_container.background =
                roundedRectWithCornerDrawable(context!!, R.color.white, 1, R.color.black, 10f)
        capture_btn.background = roundedRectWithCornerDrawable(context!!, R.color.cyan, 1, R.color.black, 10f)
        choose_gallery_btn.background = roundedRectWithCornerDrawable(context!!, R.color.cyan, 1, R.color.black, 10f)
        // Every time the provided texture view changes, recompute layout
        view_finder.addOnLayoutChangeListener { _, _, _, _, _, _, _, _, _ ->
            updateTransform()
        }


        val boardTypeAdapter =
                ArrayAdapter<String>(context!!, android.R.layout.simple_spinner_dropdown_item, boardTypeItems)

        spinner_select_board_type.setAdapter(boardTypeAdapter)
        boardType?.let {
            val indexOfBoardType = boardTypeItems.indexOf(it)
            indexOfBoardType?.let {
                spinner_select_board_type.setSelection(it)
            }
        }


        choose_gallery_btn.setOnClickListener {
            val pickImageIntent = Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            )
            startActivityForResult(pickImageIntent, SCAN_RECEIPT_GALLERY_REQUEST_CODE)
        }

    }

    private fun startCamera() {
        if (context != null) {
            viewFinder = view_finder
            val viewFinderHeight = viewFinder.height
            val viewFinderWidth = viewFinder.width
            val previewConfig = PreviewConfig.Builder().apply {
                setTargetAspectRatio(Rational(viewFinderWidth, viewFinderHeight))
                setTargetResolution(Size(viewFinderWidth, viewFinderHeight))
                // setTargetRotation(viewFinder.display.rotation)
            }.build()

            // Build the viewfinder use case
            preview = Preview(previewConfig)


            // Every time the viewfinder is updated, recompute layout
            preview?.setOnPreviewOutputUpdateListener {

                // To update the SurfaceTexture, we have to remove it and re-add it
                val parent = viewFinder.parent as ViewGroup
                parent.removeView(viewFinder)
                parent.addView(viewFinder, 0)

                viewFinder.surfaceTexture = it.surfaceTexture
                updateTransform()
            }

            // Bind use cases to lifecycle
            // If Android Studio complains about "this" being not a LifecycleOwner
            // try rebuilding the project or updating the appcompat dependency to
            // version 1.1.0 or higher.
            val imageCaptureConfig = ImageCaptureConfig.Builder()
                    .apply {
                        setTargetAspectRatio(Rational(viewFinderWidth, viewFinderHeight))
                        // We don't set a resolution for image capture; instead, we
                        // select a capture mode which will infer the appropriate
                        // resolution based on aspect ration and requested mode
                        setTargetRotation(viewFinder.display.rotation)
                        setCaptureMode(ImageCapture.CaptureMode.MAX_QUALITY)
                    }.build()

            // Build the image capture use case and attach button click listener
            imageCapture = ImageCapture(imageCaptureConfig)

//        val imageCaptureConfig = ImageCaptureConfig.Builder()
//            .setTargetRotation(windowManager.defaultDisplay.rotation)
//            .build()

            //  val imageCapture = ImageCapture(imageCaptureConfig)

            capture_btn.setOnClickListener {
                val croppedDestinationFile = makeDestinationBoardImageFile(activity!!)
                val croppedDestinationUri = Uri.fromFile(croppedDestinationFile)

                val tempOriginalFilePath = getBoardImageTempFilePath()
                val tempOriginalFile = File(tempOriginalFilePath)

                lastOriginalImageUri = Uri.fromFile(tempOriginalFile)
                val orientation = getResources().getConfiguration().orientation
                Log.i("Image", "Rotation is: ${orientation}")
                activity?.pBar?.visibility = View.VISIBLE
                imageCapture?.takePicture(tempOriginalFile,
                        object : ImageCapture.OnImageSavedListener {
                            override fun onError(
                                    error: ImageCapture.UseCaseError,
                                    message: String, exc: Throwable?
                            ) {
                                val msg = "Photo capture failed: $message"
                                activity?.toast(msg)
                                Log.e("CameraXApp", msg)
                                exc?.printStackTrace()
                                activity?.pBar?.visibility = View.GONE
                            }

                            override fun onImageSaved(file: File) {
                                doAsync {
                                    uiThread {
                                        activity?.pBar?.visibility = View.GONE
                                        lastUriUsedForDetection = croppedDestinationUri
                                        activity?.let {
                                            startCameraOnLaunch = false
                                            view_finder.visibility = View.GONE
                                            lastOriginalImageUri?.let {
                                                UCrop.of(it, croppedDestinationUri)
                                                        .start(context!!, this@CaptureImageFragment)
                                            }
                                        }
                                        // processImageFromFile(bitmapImage)
                                        //  Picasso.get().load(uri).into(snapped)
                                    }
                                }
                            }
                        })
            }
            CameraX.unbindAll()
            CameraX.bindToLifecycle(this, preview, imageCapture)
        }
    }


    private fun sendNewBoardForDisplay(newBoardCaptureData: SingleBoardCaptureData) {
        val bundle = Bundle()

        bundle.putSerializable(
                NEW_BOARD_DATA,
                newBoardCaptureData
        )

        //  snapped?.setImageBitmap(rotatedBitmap)
        model.newBoardData.postValue(newBoardCaptureData)

        findNavController().navigate(
                R.id.displayBoardResult_dest,
                bundle
        )



        lastUriUsedForDetection = null
    }


    override fun onCustomFontDialogRightBtnPressed(id: String?) {
        findNavController().popBackStack()
    }

    override fun onCustomFontDialogLeftBtnPressed(id: String?) {

    }


    private fun updateTransform() {
        val matrix = Matrix()
        view_finder?.let {
            // Compute the center of the view finder
            val xScale =
                    imageCaptureAspectRatioDenominator.toFloat() / imageCaptureAspectRatioNumerator.toFloat() * (it.width).toFloat() / it.height
            val centerX = it.width / 2f
            val centerY = it.height / 2f

            val displayRotation = it.display.rotation

            Log.i("Image", "Display rotation $displayRotation")
            // Correct preview output to account for display rotation
            val rotationDegrees = when (it.display.rotation) {
                Surface.ROTATION_0 -> 0
                Surface.ROTATION_90 -> 90
                Surface.ROTATION_180 -> 180
                Surface.ROTATION_270 -> 270
                else -> return
            }
            matrix.postRotate(-rotationDegrees.toFloat(), centerX, centerY)
            //Hack below. Struggled to get the preview to show with correct aspect ratio, position, and scale.
            matrix.preScale(xScale * 1.15f, 1f * 1.15f, centerX, centerY)
            // Finally, apply transformations to our TextureView
            view_finder.setTransform(matrix)
        }
    }


    /**
     * Process result from permission request dialog box, has the request
     * been granted? If yes, start Camera. Otherwise display a toast
     */
    override fun onRequestPermissionsResult(
            requestCode: Int, permissions: Array<String>, grantResults: IntArray
    ) {
        if (requestCode == PERMISSIONS_REQUEST_CAMERA) {
            if (allPermissionsGranted()) {
                //  view_finder?.post { startCamera() }
            } else {
                Toast.makeText(
                        context!!,
                        "Permissions not granted by the user.",
                        Toast.LENGTH_SHORT
                ).show()
            }
        } else if (requestCode == REQUEST_GALLERY_READ_PERMISSIONS) {
            if (!allGalleryPermissionsGranted()) {
                handleGalleryUri(lastUriFromGallery)
            }
        }
    }

    /**
     * Check if all permission specified in the manifest have been granted
     */
    private fun allPermissionsGranted() = REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(
                context!!, it
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun allGalleryPermissionsGranted() = GALLERY_READ_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(
                context!!, it
        ) == PackageManager.PERMISSION_GRANTED
    }


    // Used for gallery picture capture
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            SCAN_RECEIPT_GALLERY_REQUEST_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    val dataUri = data?.data
                    dataUri?.path?.let {
                        context?.let {
                            lastUriFromGallery = dataUri
                            if (!allGalleryPermissionsGranted()) {
                                // No explanation needed, we can request the permission.
                                this.requestPermissions(
                                        GALLERY_READ_PERMISSIONS,
                                        REQUEST_GALLERY_READ_PERMISSIONS
                                )
                            } else {
                                handleGalleryUri(dataUri)
                            }
                        }
                    }
                } else {

                }
            }
            UCrop.REQUEST_CROP -> {
                Log.i("Camera", "Is returning from crop in activity result: $startCameraOnLaunch")

                if (resultCode == Activity.RESULT_OK) {
                    data?.let {
                        var rotationToUse: Int = 0

                        val resultUri = UCrop.getOutput(it)

                        val file = File(resultUri?.path)
                        val bitmapImage = BitmapFactory.decodeFile(file.path)
                        bitmapImage?.let {
                            val imageWidth = it.width
                            val imageHeight = it.height

                            lastOriginalImageUri?.let {
                                val originalImageFile = File(it.toString())
                                val originalImage = MediaStore.Images.Media.getBitmap(activity?.contentResolver, it)
                                // val originalImage = BitmapFactory.decodeFile(originalImageFile.path)
                                val originalImageWidth = originalImage.width
                                val originalImageHeight = originalImage.height

                                if (imageWidth == originalImageWidth && imageHeight == originalImageHeight) {
                                    rotationToUse = 90
                                }
                            }
                            (activity as? MainActivity)?.let {
                                LineItemsGroupingAlgorithm(it).processImageFromFile(bitmapImage, rotation = rotationToUse.toFloat(), lastUriUsedForDetection = lastUriUsedForDetection,groupingCompletion = groupingAlgorithmCompletion)
                            }

                        }
                    }
                } else {
                    Log.i("Camera", "activity result not ok crop")
                    view_finder.visibility = View.VISIBLE
                    startCameraOnLaunch = true
                }
            }
            else -> {

            }

        }
    }

    private fun handleGalleryUri(dataUri: Uri?) {
        dataUri?.let {
            val rotationDegrees = getImageRotation(dataUri, context!!)
            //    dataUri.path.0
            //  Picasso.get().load(dataUri).rotate(rotationDegrees.toFloat()).into(this.chooseGalleryTargetAfterSelection)
            // val file = File(dataUri.toString())
            val croppedDestinationFile = makeDestinationBoardImageFile(activity!!)
            val croppedDestinationUri = Uri.fromFile(croppedDestinationFile)

            lastOriginalImageUri = dataUri

            UCrop.of(dataUri, croppedDestinationUri)
                    .withMaxResultSize(1500, 1500)
                    .start(context!!, this@CaptureImageFragment)

//            Picasso.get().load(dataUri).rotate(rotationDegrees.toFloat())
//                    .into(this@CaptureImageFragment.chooseGalleryTargetAfterSelection)

        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onPause() {
        //  CameraX.unbindAll()
        super.onPause()
    }

    override fun onStop() {
        super.onStop()
        CameraX.unbindAll()
    }

    override fun onStart() {
        super.onStart()
        Log.i("Camera", "onstart")


    }

    private fun startCameraOnLaunch() {
        CameraX.unbindAll()
        if (allPermissionsGranted()) {
            view_finder?.post { startCamera() }
        } else {
            this.requestPermissions(
                    cameraPermissions,
                    PERMISSIONS_REQUEST_CAMERA
            )
        }
    }

    private fun getBoardImageTempFilePath(): String {
        val storageDir = activity!!.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val imageFile = File.createTempFile(
                "scanboardoriginaltemp", /* prefix */
                ".bmp", /* suffix */
                storageDir /* directory */
        )
        return imageFile.absolutePath
    }

    companion object {
        private var startCameraOnLaunch: Boolean = true

    }
}
