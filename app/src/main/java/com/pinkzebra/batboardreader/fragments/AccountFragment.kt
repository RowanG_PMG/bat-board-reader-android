package com.pinkzebra.batboardreader.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.pinkzebra.batboardreader.MainActivity

import com.pinkzebra.batboardreader.R
import com.pinkzebra.batboardreader.models.Prefs
import com.pinkzebra.batboardreader.utils.hideKeyboard
import com.pinkzebra.batboardreader.views.CustomMenuToolbar
import kotlinx.android.synthetic.main.fragment_account.*
import org.jetbrains.anko.toast

/**
 * A simple [Fragment] subclass.
 */
class AccountFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_account, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val loggedIn = Prefs(context!!).isLoggedIn()

        if (loggedIn) {
            showLoggedInLayout()
        } else {
            showLoggedOutLayout()
        }

        login_btn.setOnClickListener {
            val teamName = team_edit_text.text.toString()
            val password = password_edit_text.text.toString()

            if ((teamName == "A" && password == "password") || teamName == "B" && password == "password") {
                var teamID: Int = 0
                when (teamName) {
                    "A" -> {
                        teamID = 1
                    }
                    "B" -> {
                        teamID = 2
                    }
                }
                Prefs(context!!).login(teamID)
                view.hideKeyboard()
                (activity as MainActivity).customNavigateToTopTab(CustomMenuToolbar.MenuItem.OUTLET)
            } else {
                activity?.toast("Sorry, login/password is incorrect.")
            }
        }

        logout_btn.setOnClickListener {
            Prefs(context!!).logout()
            showLoggedOutLayout()
            (activity as MainActivity).setNavigationClickabilityBasedOnLogin(false)
        }
    }

    private fun updateLoggedInNameUI() {
        val loggedInTeamID = Prefs(context!!).getLoginID()

        loggedInTeamID?.let {
            val teamName = getTeamNameFromTeamID(it)
            logged_in_text.text = "You are logged in as team: ${teamName}"
        }

    }

    private fun showLoggedInLayout() {
        login_form.visibility = View.GONE
        already_logged_in.visibility = View.VISIBLE

        updateLoggedInNameUI()
    }

    private fun showLoggedOutLayout() {

        login_form.visibility = View.VISIBLE
        already_logged_in.visibility = View.GONE
    }

    private fun getTeamNameFromTeamID(teamID: Int): String {
        when (teamID) {
            1 -> return "A"
            2 -> return "B"
            else -> {
                return "Unknown"
            }
        }
    }
}
