package com.pinkzebra.batboardreader.fragments


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.widget.ArrayAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.pinkzebra.batboardreader.LocationRequests
import com.pinkzebra.batboardreader.MainActivity

import com.pinkzebra.batboardreader.R
import com.pinkzebra.batboardreader.adapters.VenueListAdapter
import com.pinkzebra.batboardreader.adapters.VenueListModel
import com.pinkzebra.batboardreader.views.roundedRectWithCornerDrawable
import kotlinx.android.synthetic.main.fragment_venue_mapped_pricing.*
import kotlinx.android.synthetic.main.fragment_venue_mapped_pricing.view.*
import android.view.inputmethod.EditorInfo

import android.widget.TextView
import android.widget.TextView.OnEditorActionListener
import android.content.ContentValues.TAG
import android.content.pm.PackageManager
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.widget.AdapterView
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.pinkzebra.batboardreader.PERMISSIONS_REQUEST_LOCATION_VENUE
import com.pinkzebra.batboardreader.adapters.VenueListItemListener
import com.pinkzebra.batboardreader.models.*
import com.pinkzebra.batboardreader.utils.distanceBetweenPoints
import com.pinkzebra.batboardreader.utils.hideKeyboard
import com.pinkzebra.batboardreader.utils.showButtonDisabled
import com.pinkzebra.batboardreader.utils.showButtonEnabled
import kotlinx.android.synthetic.main.product_drop_down_selectors.*
import kotlinx.android.synthetic.main.product_drop_down_selectors.view.*
import org.jetbrains.anko.toast

class VenueMappedPricingFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnCameraMoveStartedListener,
        VenueListItemListener {
    private var googleMap: GoogleMap? = null
    private var locationRequest: LocationRequests? = null

    private var lastLocation: LatLng? = null
    private val markerArray: MutableList<Marker> = mutableListOf()

    private var allTeamVenueListData: MutableList<VenueListModel> = mutableListOf()
    private var venueListDataMatchingProductID: MutableList<VenueListModel> = mutableListOf()

    private var venueListDataToDisplay: MutableList<VenueListModel> = mutableListOf()

    private var categories: List<LocalRepository.ImportedCategoriesStructure> = listOf()
    private var manufacturers: List<Manufacturer> = listOf()
    private var products: List<Product> = listOf()

    private lateinit var venuesListAdapter: RecyclerView.Adapter<*>
    private lateinit var productTypeAdapter: ArrayAdapter<String>
    private lateinit var manufacturerAdapter: ArrayAdapter<String>
    private lateinit var categoriesAdapter: ArrayAdapter<String>

    private var sortedProductItemsByName: MutableList<Product> = mutableListOf()

    private var lastSelectedVenueIndexInDisplayedList: Int = 0
    private var lastSelectedProductID: Int = 0

    data class VenuePricingDataStatus(
            val mapReady: Boolean = false,
            val venueDataReady: Boolean = false,
            val locationReady: Boolean = false,
            val markersSet: Boolean = false,
            val dataSorted: Boolean = false,
            val haveShownLocation: Boolean = false,
            val productDataReady: Boolean = false
    )

    data class VenueProductPrice(val venueID: Int, val price: Float)

    private var mapDataStatus = VenuePricingDataStatus()
        set(value) {
            field = value
            updateVenuesTableDistancesIfDataAndLocationReady()

            // addMapMarkersIfDataAndMapReady()
            showCurrenLocationIfMapAndLocationReady()
            setProductSelectorEnabledOrDisabledIfProductAndVenueDataReady()
        }

    private fun showCurrenLocationIfMapAndLocationReady() {
        if (mapDataStatus.mapReady && mapDataStatus.locationReady && !mapDataStatus.haveShownLocation) {
            showCurrentLocation()
            mapDataStatus = mapDataStatus.copy(haveShownLocation = true)
        }
    }

    private fun showCurrentLocation() {
        if (ContextCompat.checkSelfPermission(
                        activity!!,
                        android.Manifest.permission.ACCESS_FINE_LOCATION
                )
                == PackageManager.PERMISSION_GRANTED
        ) {
            googleMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(lastLocation, 11f))
            googleMap?.isMyLocationEnabled = true
        }
    }

    private fun updateVenuesTableDistancesIfDataAndLocationReady() {
        if (mapDataStatus.locationReady && mapDataStatus.venueDataReady) {
            allTeamVenueListData = allTeamVenueListData.map {
                val venueDistance = distanceBetweenPoints(lastLocation!!, it.latLng).toFloat()
                val venueDistanceTrimmedDecimals = (Math.round(venueDistance * 100) / 100.0).toFloat()
                it.copy(venueDistance = venueDistanceTrimmedDecimals)
            }.toMutableList()


            //  sortVenuesToDisplayByDistance()
            //   updateVenueDetailTab(lastSelectedVenueIndexInDisplayedList)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        locationRequest = LocationRequests(activity = activity as MainActivity, fragment = this) {
            it?.let { latLng ->
                setCurrentLocation(latLng)
                Log.i("Map", latLng.toString())
            } ?: run {
                Log.i("Map", "Location not found")
            }
        }

        locationRequest?.checkLocationPermissionAndGetLocation()

        // fetchLatestProductPriceAtTeamVenuesForProductID(productID = 130)
        loadCategories()
        loadManufacturers()

    }

    private fun fetchLatestProductPriceAtTeamVenuesForProductID(productID: Int, completion: ((List<VenueProductPrice>) -> Unit)? = null) {
        val allVenuesIDsforTeam = allTeamVenueListData.map {it.venueID }
        FirestoreRepository().fetchVenuesRecentPricesMatchingProductIDAndTeamVenues(productID = productID, teamVenueIDs = allVenuesIDsforTeam,completionHandler = {
            updateProductNameAndPriceRangeData(it, productID)
            updateVenuesDataWithVenuePrices(it, allTeamVenueListData)
            sortVenuesToDisplayByDistance()
            updateVenueListDataToDisplayBasedOnNotNullPrice()
            venuesListAdapter.notifyDataSetChanged()
            venue_detail_strip.visibility = View.VISIBLE
            updateVenueDetailTab(0)
            addMapMarkersBasedOnVenuesListToDisplay()
        })
    }

    private fun updateProductNameAndPriceRangeData(list: List<VenueProductPrice>, productID: Int?) {

        val product = products.find { it.productID == productID }
        val productName = product?.name
        val productSize = product?.size
        if (productName != null) {
            product_name.text = "${productName.toUpperCase()} $productSize"
        } else {
            product_name.text = "No Product Selected"
        }

        val minPriceVenueProduct = list.minBy { it.price }
        val minPrice = minPriceVenueProduct?.price
        val minPriceFormatted = "%.2f".format(minPrice)
        val venueAtMinPrice = allTeamVenueListData.find { it.venueID == minPriceVenueProduct?.venueID }
        val venueNameMinPrice = venueAtMinPrice?.venueName
        if (minPrice != null && venueNameMinPrice != null) {
            product_min_price_at_venue.text = "Min: $$minPriceFormatted @ $venueNameMinPrice"
        } else {
            product_min_price_at_venue.text = "Min:"
        }

        val maxPriceVenueProduct = list.maxBy { it.price }
        val maxPrice = maxPriceVenueProduct?.price
        val maxPriceFormatted = "%.2f".format(maxPrice)
        val venueAtMaxPrice = allTeamVenueListData.find { it.venueID == maxPriceVenueProduct?.venueID }
        val venueNameMaxPrice = venueAtMaxPrice?.venueName
        if (maxPrice != null && venueNameMaxPrice != null) {
            product_max_price_at_venue.text = "Max: $$maxPriceFormatted @ $venueNameMaxPrice"
        } else {
            product_max_price_at_venue.text = "Max:"
        }

        val floatPriceList = list.map { it.price }
        val medianPrice = medianFromList(floatPriceList)
        val medianPriceFormatted = "%.2f".format(medianPrice)
        if (medianPrice != null) {
            product_median_price_at_venue.text = "Median: $$medianPriceFormatted"
        } else {
            product_median_price_at_venue.text = "Median:"
        }

    }

    private fun medianFromList(list: List<Float>): Float? {
        val sortedList = list.sorted()
        val sortedListCount = sortedList.count()

        if (sortedListCount == 0) {
            return null
        }
        if (sortedListCount % 2 == 0) {
            val leftMiddleVal = sortedList[sortedListCount / 2 - 1]
            val rightMiddleVal = sortedList[sortedListCount / 2]
            return (leftMiddleVal + rightMiddleVal) / 2
        } else {
            return sortedList[sortedListCount / 2]
        }
    }

    private fun loadCategories() {
        categories = LocalRepository().getCategoriesJson(activity!!)
    }

    private fun populateCategoriesSelector() {
        val categoriesStrings = categories.mapNotNull {
            it.type?.toUpperCase()
        }

        categoriesAdapter = ArrayAdapter<String>(context!!, android.R.layout.simple_spinner_dropdown_item, categoriesStrings)
        product_dropdown_selectors_venue_pricing.generic_package_type_selector.adapter = categoriesAdapter
        categoriesAdapter.notifyDataSetChanged()
    }

    private fun loadManufacturers() {
        manufacturers = LocalRepository().getManufacturersJson(activity!!)
    }

    private fun populateManufacturersSelector() {
        val manufacturersStrings = manufacturers.mapNotNull {
            it.name.toUpperCase()
        }

        manufacturerAdapter = ArrayAdapter<String>(context!!, android.R.layout.simple_spinner_dropdown_item, manufacturersStrings)
        product_dropdown_selectors_venue_pricing.manufacturer_selector.adapter = manufacturerAdapter
        manufacturerAdapter.notifyDataSetChanged()
    }


    private fun fetchAllVenueData() {
        allTeamVenueListData.clear()
        val teamID = Prefs(context!!).getLoginID()
        teamID?.let {
            FirestoreRepository().fetchVenueData(teamID = it,
                    completionHandler = {
                        val venueListData = it.mapIndexed { index, item ->
                            VenueListModel(
                                    venueID = item.venueID,
                                    markerID = index,
                                    venueName = item.venueName,
                                    address = item.address,
                                    latLng = item.latLng,
                                    isFavorite = false,
                                    boardType = "",
                                    productPrice = null,
                                    venueDistance = 0f
                            )

                        }

                        this.allTeamVenueListData.addAll(venueListData)
//                    venueListDataToDisplay.addAll(venueListDataMatchingProductID)
//                    venuesListAdapter.notifyDataSetChanged()
                        mapDataStatus = mapDataStatus.copy(venueDataReady = true)

                    },
                    failHandler = {

                    }
            )
        }
    }

    private fun fetchAllProducts() {
        FirestoreRepository().fetchProductData(completionHandler = {
            this.products = it
            this.sortedProductItemsByName = it.sortedBy { it.name }.toMutableList()
            Log.i("Data", "Products: " + it.toString())
            mapDataStatus = mapDataStatus.copy(productDataReady = true)

        })
    }

    override fun onResume() {
        super.onResume()

    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_venue_mapped_pricing, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        search_text.setText("")
        venue_detail_strip.visibility = View.GONE

        updateProductNameAndPriceRangeData(listOf(),null)

        (childFragmentManager.findFragmentById(R.id.map_venue) as SupportMapFragment).getMapAsync(this)

        search_box_container.background =
                roundedRectWithCornerDrawable(context!!, R.color.white, 1, R.color.black, 5f)
        select_product_btn.background =
                roundedRectWithCornerDrawable(context!!, R.color.cyan, 1, R.color.black, 5f)
        venue_detail_strip.view_detail_btn.background =
                roundedRectWithCornerDrawable(context!!, R.color.cyan, 1, R.color.black, 5f)

        //  venueListDataToDisplay.addAll(venueListDataMatchingProductID)
        venuesListAdapter = VenueListAdapter(venueListDataToDisplay, showPricing = true, listener = this)
        venue_items_recycler.apply {
            // use this setting to improve performance if you know that changes
            // in captureMechanism do not change the layout size of the RecyclerView
            setHasFixedSize(false)
            layoutManager = LinearLayoutManager(context)
            adapter = venuesListAdapter
            this.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        }

        search_text.setOnEditorActionListener(object : OnEditorActionListener {
            override fun onEditorAction(v: TextView, actionId: Int, event: KeyEvent?): Boolean {
                if (event != null && (event!!.getKeyCode() === KeyEvent.KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_DONE)) {
                    Log.i(TAG, "Enter pressed")
                    search_text.clearFocus()
                    this@VenueMappedPricingFragment.view?.hideKeyboard()
                }

                return false
            }
        })

        search_text.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                venueListDataToDisplay.clear()
                val newFilteredVenues = venueListDataMatchingProductID.filter {
                    it.venueName.toLowerCase().contains(search_text.text.toString().toLowerCase())
                }
                venueListDataToDisplay.addAll(newFilteredVenues)
                venuesListAdapter.notifyDataSetChanged()
                addMapMarkersIfDataAndMapReady()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })

        sort_by_nearest.setOnClickListener {
            sort_by_nearest.setTextColor(resources.getColor(R.color.cyan))
            sort_by_price.setTextColor(resources.getColor(R.color.black))
            sortVenuesToDisplayByDistance()
        }

        sort_by_price.setOnClickListener {
            sort_by_nearest.setTextColor(resources.getColor(R.color.black))
            sort_by_price.setTextColor(resources.getColor(R.color.cyan))
            sortVenuesToDisplayByPrice()
        }

        view_detail_btn.setOnClickListener {
            activity!!.toast("Venue detail not yet available")
        }

        select_product_btn.setOnClickListener {
            showProductSelectorMode()
            populateProductDropdownSelector(sortedProductItemsByName)
        }

        cancel_select_product.setOnClickListener {

            showVenuePricingMode()
        }

        ok_select_product.setOnClickListener {
            val selectedProductID = sortedProductItemsByName[brand_product_selector.selectedItemPosition].productID
            selectedProductID?.let {
                lastSelectedProductID = it
                fetchLatestProductPriceAtTeamVenuesForProductID(productID = selectedProductID)
            }
            showVenuePricingMode()
        }

        cancel_select_product.background =
                roundedRectWithCornerDrawable(context!!, R.color.cyan, 1, R.color.black, 5f)

        ok_select_product.background =
                roundedRectWithCornerDrawable(context!!, R.color.cyan, 1, R.color.black, 5f)

        manufacturer_selector.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                filterProductsBasedOnPackageAndManufacturerSelected()
                populateProductDropdownSelector(this@VenueMappedPricingFragment.sortedProductItemsByName)
            }
        }



        generic_package_type_selector.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                filterProductsBasedOnPackageAndManufacturerSelected()
                populateProductDropdownSelector(this@VenueMappedPricingFragment.sortedProductItemsByName)
            }
        }

        fetchAllVenueData()
        fetchAllProducts()

        populateCategoriesSelector()
        populateManufacturersSelector()
        //  FirestoreRepository().uploadAllVenueJSONDataToFirestore(activity!!)
    }

    private fun showProductSelectorMode() {
        product_dropdown_selectors_venue_pricing.visibility = View.VISIBLE
        venue_product_area.visibility = View.GONE
    }

    private fun showVenuePricingMode() {
        product_dropdown_selectors_venue_pricing.visibility = View.GONE
        venue_product_area.visibility = View.VISIBLE
    }

    private fun sortVenuesToDisplayByPrice() {
        venueListDataToDisplay.clear()

        val sortedByNearest = venueListDataMatchingProductID.sortedBy {
            it.productPrice
        }

        venueListDataToDisplay.addAll(sortedByNearest)
        venuesListAdapter.notifyDataSetChanged()
    }

    private fun sortVenuesToDisplayByDistance() {
        venueListDataToDisplay.clear()

        val sortedByNearest = venueListDataMatchingProductID.sortedBy {
            it.venueDistance
        }

        venueListDataToDisplay.addAll(sortedByNearest)
        venuesListAdapter.notifyDataSetChanged()
    }

//    private fun updateVenuesListDisplayedBasedOnSearchTermAndProductID(searchTerm: String, productID: Int) {
//        val newFilteredVenues = venueListDataMatchingProductID.filter {
//            it.venueName.toLowerCase().contains(searchTerm.toString().toLowerCase())
//        }
//        venueListDataToDisplay.addAll(newFilteredVenues)
//        venuesListAdapter.notifyDataSetChanged()
//        addMapMarkersIfDataAndMapReady()
//    }


    override fun cameraCaptureClicked(position: Int) {
        Log.i(TAG, "Camera clicked at position $position")
        val bundle = Bundle()
        bundle.putSerializable(VENUE_NAME, venueListDataToDisplay[position].venueName)
        bundle.putSerializable(BOARD_TYPE, venueListDataToDisplay[position].boardType)
        bundle.putSerializable(VENUE_ID, venueListDataToDisplay[position].venueID)
        findNavController().navigate(R.id.captureImage_dest, bundle)
    }

    private fun setCurrentLocation(latLng: LatLng) {
        lastLocation = latLng
        mapDataStatus = mapDataStatus.copy(locationReady = true)
    }

    private fun addMapMarkersIfDataAndMapReady() {
        if (mapDataStatus.venueDataReady && mapDataStatus.mapReady && !mapDataStatus.markersSet) {
            clearAllMapMarkers()
            addMapMarkersBasedOnVenuesListToDisplay()
            mapDataStatus = mapDataStatus.copy(markersSet = true)
        }
    }

    private fun addMapMarkersBasedOnVenuesListToDisplay() {
        clearAllMapMarkers()
        for (i in 0 until venueListDataToDisplay.count()) {
            val mapItem = venueListDataToDisplay[i]
            val marker = googleMap!!.addMarker(
                    MarkerOptions().position(mapItem.latLng)
                            .title(mapItem.venueName)
            )
//                val possibleCustomMarker = getCustomMapMarker()
//                possibleCustomMarker?.let {
//                    marker.setIcon(it)
//                }

            marker?.tag = mapItem.markerID
            markerArray.add(marker)
        }
    }

    private fun clearAllMapMarkers() {
        googleMap?.clear()
        markerArray.clear()
    }

    override fun listItemClicked(position: Int) {
        Log.i(TAG, "Venue clicked a position $position")
        updateVenueDetailTab(position)

        (venuesListAdapter as VenueListAdapter?)?.selectedVenueIndex = position
        venuesListAdapter.notifyDataSetChanged()
        lastSelectedVenueIndexInDisplayedList = position
        val venue = venueListDataToDisplay[position]
        val dataID = venue.markerID
        val associatedMarker = markerArray.firstOrNull {
            it.tag == dataID
        }
        associatedMarker?.showInfoWindow()
        googleMap?.animateCamera(
                CameraUpdateFactory.newLatLngZoom(
                        associatedMarker?.position,
                        googleMap!!.cameraPosition.zoom
                )
        )
    }

    override fun onCameraMoveStarted(p0: Int) {

    }

    override fun onMapReady(p0: GoogleMap?) {
        this.googleMap = p0
        mapDataStatus = mapDataStatus.copy(mapReady = true)

        googleMap?.setOnMarkerClickListener { marker ->
            markerClicked(marker)
            false
        }
    }


    fun markerClicked(marker: Marker) {
        val markerId = marker.tag as Int
        val associatedDataIndex = venueListDataToDisplay.indexOfFirst {
            markerId == it.markerID
        }

        if (associatedDataIndex >= 0) {
            activateAndScrollInListOfVenuesUponMarkerClick(associatedDataIndex)
            updateVenueDetailTab(associatedDataIndex)
            lastSelectedVenueIndexInDisplayedList = associatedDataIndex
        }
    }

    open fun activateAndScrollInListOfVenuesUponMarkerClick(associatedDataIndex: Int) {
        val venueRecyclerView = venue_items_recycler
        listItemClicked(associatedDataIndex)
        venueRecyclerView?.smoothScrollToPosition(associatedDataIndex)
    }

    private fun filterProductsBasedOnPackageAndManufacturerSelected() {

        val selectedManufacturerID = manufacturers[manufacturer_selector.selectedItemPosition].id
        val selectedCategoriesID = categories[generic_package_type_selector.selectedItemPosition].id

        val filteredSortedProducts = products.filter {
            it.manufacturerID == selectedManufacturerID && it.packageType_id == selectedCategoriesID
        }.sortedBy { it.name }

        this.sortedProductItemsByName = filteredSortedProducts.toMutableList()
    }

    private fun populateProductDropdownSelector(sortedProductData: List<Product>) {
        val sortedProductNames = sortedProductData.map { "${it.name} ${it.size}" }
        productTypeAdapter = ArrayAdapter<String>(context!!, android.R.layout.simple_spinner_dropdown_item, sortedProductNames)
        product_dropdown_selectors_venue_pricing.brand_product_selector.adapter = productTypeAdapter
        productTypeAdapter.notifyDataSetChanged()
    }

    fun updateVenueDetailTab(positionInDataArray: Int) {
        if (venueListDataToDisplay.count() >= 1) {
            if (venue_detail_strip != null) { //Bug due to device rotation request
                venue_detail_strip.visibility = View.VISIBLE
                val venue = venueListDataToDisplay[positionInDataArray]
                if (venue_name_detail_strip != null) {
                    venue_name_detail_strip?.text = venue.venueName
                }
                venue_address_and_phone_detail_strip?.text = venue.address
            }
        } else {
            venue_detail_strip.visibility = View.GONE
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSIONS_REQUEST_LOCATION_VENUE -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    // permission was granted
                    locationRequest?.getLocation()
                } else {
                    // permission denied
                    Log.i("Location", "Location not called. Result lacks necessary info.")
                    val sydneyLocation = LatLng(-33.8688, 151.2093)
                    setCurrentLocation(sydneyLocation)
                }
                return
            }

            else -> {
                // Ignore all other requests.
                Log.i("Location", "Other permission result received.")
            }
        }
    }

    private fun updateVenueListToDisplayWithNewVenuePricesForProductID(list: List<VenueProductPrice>) {
        venueListDataToDisplay.clear()
        val mappedVenuesToDisplay = list.mapNotNull { venueProductPrice ->
            venueProductPrice
            val matchingVenue = allTeamVenueListData.firstOrNull {
                it.venueID == venueProductPrice.venueID
            }
            matchingVenue
        }

        venueListDataToDisplay.addAll(mappedVenuesToDisplay)
        venuesListAdapter.notifyDataSetChanged()
    }

    private fun updateVenuesDataWithVenuePrices(venueListWithMatchingProductPrices: List<VenueProductPrice>, existingVenueData: List<VenueListModel>) {
        val updatedVenueListPriceData: MutableList<VenueListModel> = mutableListOf()
        for (venue in existingVenueData) {
            var priceToUse: Float? = null
            val matchingVenuePrice = venueListWithMatchingProductPrices.firstOrNull { it.venueID == venue.venueID }
            matchingVenuePrice?.price?.let {
                priceToUse = it
                updatedVenueListPriceData.add(venue.copy(productPrice = priceToUse))
            }
        }
        this.venueListDataMatchingProductID.clear()
        this.venueListDataMatchingProductID.addAll(updatedVenueListPriceData)
    }

    private fun updateVenueListDataToDisplayBasedOnNotNullPrice() {
        val venuesListToDisplayNonNullPrice = venueListDataToDisplay.filter {
            it.productPrice ?: 0f > 0f
        }
        venueListDataToDisplay.clear()
        venueListDataToDisplay.addAll(venuesListToDisplayNonNullPrice)
    }

    private fun setProductSelectorEnabledOrDisabledIfProductAndVenueDataReady() {
        if (mapDataStatus.venueDataReady && mapDataStatus.productDataReady) {
            select_product_btn.isEnabled = true
            showButtonEnabled(select_product_btn, context!!)
        } else {
            select_product_btn.isEnabled = false
            showButtonDisabled(select_product_btn, context!!)
        }
    }
}
