package com.pinkzebra.batboardreader.views

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.util.AttributeSet
import android.view.View
import androidx.core.content.ContextCompat
import com.pinkzebra.batboardreader.R

class ShowBoundingBoxOnPreview(context: Context, attrs: AttributeSet?) : View(context, attrs) {
    private val linePaint = Paint()
    private var minX: Int = 0
    private var maxX: Int = 0
    private var minY: Int = 0
    private var maxY: Int = 0

    private var boxes = mutableListOf<Rect>()

    init {
        val color = ContextCompat.getColor(context, R.color.orange_20pc)
        linePaint.setColor(color)
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        for (box in boxes) {
           // canvas?.drawRect(minX.toFloat(), minY.toFloat(), maxX.toFloat(), maxY.toFloat(), linePaint)
            canvas?.drawRect(box, linePaint)
        }
    }

    fun setBoundingCorners(left: Int, top: Int, right: Int, bottom: Int) {
        this.minX = left
        this.minY= top
        this.maxX = right
        this.maxY = bottom
        this.invalidate()
    }

    fun setBoxes(rects: List<Rect>) {
        this.boxes = rects.toMutableList()
        this.invalidate()
    }
}