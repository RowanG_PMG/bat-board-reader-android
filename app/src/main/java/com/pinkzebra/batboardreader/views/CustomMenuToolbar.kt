package com.pinkzebra.batboardreader.views

object CustomMenuToolbar {
    var currentMenuItem: MenuItem = CustomMenuToolbar.MenuItem.OUTLET

    enum class MenuItem {
        OUTLET, PRODUCTS, REPORTS, ACCOUNT
    }
}