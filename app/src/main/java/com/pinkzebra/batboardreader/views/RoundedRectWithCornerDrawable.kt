package com.pinkzebra.batboardreader.views

import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.content.res.ColorStateList
import androidx.core.content.ContextCompat

fun roundedRectWithCornerDrawable(context: Context, backgroundColor: Int, strokeWidth: Int, strokeColor: Int, cornerRadius: Float): GradientDrawable {

    val gradDrawable = GradientDrawable()
    gradDrawable.shape = GradientDrawable.RECTANGLE

    val backColor = ContextCompat.getColor(context, backgroundColor)
    val backColorCsl = ColorStateList.valueOf(backColor)
    gradDrawable.color = backColorCsl

    val strokeColorContext = ContextCompat.getColor(context, strokeColor)
    val strokeColorCsl = ColorStateList.valueOf(strokeColorContext)
    gradDrawable.setStroke(strokeWidth, strokeColorCsl)

    gradDrawable.cornerRadius = cornerRadius
    return gradDrawable
}