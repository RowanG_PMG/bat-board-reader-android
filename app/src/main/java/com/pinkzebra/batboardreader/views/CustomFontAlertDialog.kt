package com.pinkzebra.batboardreader.views

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.text.SpannableStringBuilder
import android.view.ContextThemeWrapper
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.pinkzebra.batboardreader.R
import kotlinx.android.synthetic.main.layout_custom_font_alert_dialog.view.*

class CustomFontAlertDialog(context: Context, val listener: DialogListener?, title: String? = null, message: String? = null, spannableMessage: SpannableStringBuilder?=null, val hasLeftBtn: Boolean? = null, rightBtnText: String? = null, leftBtnText: String? = null,val id: String? = null) : AlertDialog.Builder(
    ContextThemeWrapper(context, R.style.ThemeOverlay_AppCompat)
) {

    interface DialogListener {
        fun onCustomFontDialogRightBtnPressed(id: String? = null)
        fun onCustomFontDialogLeftBtnPressed(id: String? = null)
    }

    private var messageTextView: TextView? = null
    private var dialogView: LinearLayout? = null

    init {
        @SuppressLint("InflateParams") // It's OK to use NULL in an AlertDialog it seems...
        // val inflater = View.inflate(context)
        dialogView = View.inflate(context, R.layout.layout_custom_font_alert_dialog, null) as LinearLayout
        messageTextView = dialogView?.message
        setView(dialogView)

        title?.let {
            dialogView?.title?.text = it
        }

        if (message != null) {
            dialogView?.message?.text = message
        } else if (spannableMessage != null) {
            setMessage(spannableMessage)
        }

        rightBtnText?.let {
            dialogView?.rightBtn?.text = it
        }

        leftBtnText?.let {
            dialogView?.leftBtn?.text = it
        }
    }

    override fun show(): AlertDialog {
        val dialog = super.show()
        dialog.setCanceledOnTouchOutside(false)
        dialogView?.rightBtn?.setOnClickListener {
            listener?.onCustomFontDialogRightBtnPressed(id=id)
            dialog.dismiss()
        }
        if (hasLeftBtn == true) {
            dialogView?.leftBtn?.setOnClickListener {
                listener?.onCustomFontDialogLeftBtnPressed(id=id)
                dialog.dismiss()
            }
        } else {
            dialogView?.leftBtn?.visibility = View.GONE
        }
        return dialog
    }

    private fun setMessage(message: SpannableStringBuilder) {
        messageTextView?.text = message
    }

}