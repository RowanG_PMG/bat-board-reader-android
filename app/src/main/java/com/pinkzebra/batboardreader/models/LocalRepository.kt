package com.pinkzebra.batboardreader.models

import android.app.Activity
import com.google.common.io.ByteStreams
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*

class LocalRepository {

    data class ImportedKeywordStructure(val KEYWORD: String?)

    data class ImportedManufacturersStructure(@SerializedName("manufacturer_id") val id: Int?=null, @SerializedName("manufacturer") val name:String? = null): Serializable

    data class ImportedCategoriesStructure(@SerializedName("type_id") val id: Int?=null, @SerializedName("type") val type:String? = null, @SerializedName("code") val code:String? = null, @SerializedName("description") val description:String? = null): Serializable

    fun getKeywordsJson(activity: Activity): List<String> {
        val gson = Gson()
        val inputStream = activity!!.assets.open("batdata/keywords.json")
        val jsonKeywords = String(ByteStreams.toByteArray(inputStream))

        val jsonKeywordsObject = gson.fromJson(jsonKeywords, Array<ImportedKeywordStructure>::class.java)

        val keywords = jsonKeywordsObject.mapNotNull {
            it.KEYWORD
        }
        return keywords.toList()
    }

    fun getManufacturersJson(activity: Activity): List<Manufacturer> {
        val gson = Gson()
        val inputStream = activity!!.assets.open("batdata/manufacturers.json")
        val jsonManufacturers = String(ByteStreams.toByteArray(inputStream))

        val jsonManufacturersObject = gson.fromJson(jsonManufacturers, Array<ImportedManufacturersStructure>::class.java)

        val manufacturers = jsonManufacturersObject.mapNotNull {
            if (it.name != null && it.id != null) {
                Manufacturer(name = it.name, id = it.id)
            } else { null }
        }
        return manufacturers
    }

    fun getCategoriesJson(activity: Activity): List<ImportedCategoriesStructure> {
        val gson = Gson()
        val inputStream = activity!!.assets.open("batdata/categories.json")
        val jsonCategories = String(ByteStreams.toByteArray(inputStream))

        val jsonCategoriesObject = gson.fromJson(jsonCategories, Array<ImportedCategoriesStructure>::class.java)

        return jsonCategoriesObject.toList()
    }

}