package com.pinkzebra.batboardreader.models

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class CameraAndDisplayBoardSharedViewModel : ViewModel() {
    val newBoardData = MutableLiveData<SingleBoardCaptureData?>()

    fun doSomething() {

    }
}