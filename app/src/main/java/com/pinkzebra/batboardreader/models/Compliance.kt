package com.pinkzebra.batboardreader.models

fun passesCompliance(manufacturerID: Int?, boardPosition: Int, boardType: String? = null): Boolean {
    if (boardPosition <= 3) {
        if (manufacturerID == AppConstants.batManufacturerID) {
            return true
        } else {
            return false
        }
    } else {
        return true
    }
}