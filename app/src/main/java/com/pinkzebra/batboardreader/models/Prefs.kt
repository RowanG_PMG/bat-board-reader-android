package com.pinkzebra.batboardreader.models

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson

const val PRODUCT_PREFS_FILENAME = "com.pinkzebra.apps.bat_board_reader.board_history"
const val BOARD_HISTORY_KEY = "BoardHistory"

const val LOGIN_PREFS_FILENAME = "com.pinkzebra.apps.bat_board_reader.login"
const val LOGIN_KEY = "Login"

class Prefs(context: Context) {
    private var boardHistoryPrefs: SharedPreferences? = context.getSharedPreferences(PRODUCT_PREFS_FILENAME, 0)
    private var loginPrefs: SharedPreferences? = context.getSharedPreferences(LOGIN_PREFS_FILENAME, 0)

    fun login(teamID: Int) {
        val prefsEditor = loginPrefs!!.edit()
        prefsEditor.putInt(LOGIN_KEY, teamID)
        prefsEditor.apply()
    }

    fun logout() {
        val prefsEditor = loginPrefs!!.edit()
        prefsEditor.putString(LOGIN_KEY, null)
        prefsEditor.apply()
    }

    fun getLoginID(): Int? {
        val loginID =  loginPrefs?.getInt(LOGIN_KEY, -1)
        return loginID
    }

    fun isLoggedIn(): Boolean {
        if (getLoginID() != -1) {
            return true
        } else {
            return false
        }
    }

    fun updateProductPrices(products: SingleBoardCaptureData) {
        val gson = Gson()
        val json = gson.toJson(products)
        val prefsEditor = boardHistoryPrefs!!.edit()
        prefsEditor.putString(BOARD_HISTORY_KEY, json)
        prefsEditor.apply()
    }

    fun getProductPrices(): SingleBoardCaptureData? {
        val gson = Gson()
        val json = boardHistoryPrefs?.getString(BOARD_HISTORY_KEY, "")
        val productPrices = gson.fromJson(json, SingleBoardCaptureData::class.java)
        return productPrices
    }

    fun getBoardHistory(): BoardCaptureDataHistory? {
        val gson = Gson()
        val json = boardHistoryPrefs?.getString(BOARD_HISTORY_KEY, "")
        val productPrices = gson.fromJson(json, BoardCaptureDataHistory::class.java)
        return productPrices
    }

    fun addNewBoardToHistory(board: SingleBoardCaptureData) {
        val history = getBoardHistory()?.itemsHistory?.toMutableList() ?: mutableListOf()
        history.add(board)

        val updatedHistory = BoardCaptureDataHistory(itemsHistory = history)
        val gson = Gson()
        val json = gson.toJson(updatedHistory)
        val prefsEditor = boardHistoryPrefs!!.edit()
        prefsEditor.putString(BOARD_HISTORY_KEY, json)
        prefsEditor.apply()
    }
}