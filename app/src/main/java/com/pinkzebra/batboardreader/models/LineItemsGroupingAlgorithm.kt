package com.pinkzebra.batboardreader.models

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Matrix
import android.net.Uri
import android.util.Log
import android.view.View
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.text.FirebaseVisionText
import com.pinkzebra.batboardreader.MainActivity
import com.pinkzebra.batboardreader.utils.getStringTimeStampWithDate
import com.pinkzebra.batboardreader.utils.makeDestinationBoardImageFile
import com.pinkzebra.batboardreader.utils.replaceWhiteSpaceWith
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.io.File
import java.io.FileOutputStream
import java.util.*

class LineItemsGroupingAlgorithm(val activity: MainActivity) {
    var ocrImageHeight: Int = 0
    val ocrImageWidth = 1500
    var thresholdDifferenceInHeightForLineItemsMatch = Math.tan(1.2 * Math.PI / 180) * ocrImageWidth * 0.7

    var batProductKeyWords: MutableList<String> = mutableListOf()

    val detector = FirebaseVision.getInstance()
            .onDeviceTextRecognizer

    init {
        batProductKeyWords = LocalRepository().getKeywordsJson(activity = activity).map { it.toLowerCase() }.toMutableList()
    }

    fun processImageFromFile(bitmapImage: Bitmap, rotation: Float = 90f, lastUriUsedForDetection: Uri?, groupingCompletion: ((SingleBoardCaptureData) -> Unit)) {
        activity!!.pBar.visibility = View.VISIBLE
        var lastUriUsedForDetection = lastUriUsedForDetection
        doAsync {
            val matrix = Matrix()
            matrix.postRotate(rotation)

            ocrImageHeight = ocrImageWidth * bitmapImage.height / bitmapImage.width
            val scaledBitmap =
                    Bitmap.createScaledBitmap(bitmapImage, ocrImageWidth, ocrImageHeight, false)
            val rotatedBitmap = Bitmap.createBitmap(
                    scaledBitmap,
                    0,
                    0,
                    scaledBitmap.width,
                    scaledBitmap.height,
                    matrix,
                    true
            )

            if (lastUriUsedForDetection == null) {
                val file = makeDestinationBoardImageFile(activity)
                lastUriUsedForDetection = Uri.fromFile(file)
            }
            val scaledImgFile = File(lastUriUsedForDetection?.path)

            if (scaledImgFile != null) {
                val out = FileOutputStream(scaledImgFile, false)
                rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 30, out)
                out.flush()
                out.close()

                val intent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, lastUriUsedForDetection)
                activity!!.sendBroadcast(intent)
            }

            val firebaseImage = FirebaseVisionImage.fromBitmap(rotatedBitmap)


            detector.processImage(firebaseImage)
                    .addOnCompleteListener { visionText ->
                        uiThread {
                            if (activity != null) {
                                activity?.pBar?.visibility = View.GONE
                                visionText.result?.text.let {

                                    Log.i("Image", "Vision OCR Text is $it")
                                    val arrayLinesMatchingBATProductKeywords: MutableList<FirebaseVisionText.Line> =
                                            mutableListOf()

                                    val allLines: MutableList<FirebaseVisionText.Line> = mutableListOf()

                                    for (block in visionText.result!!.textBlocks) {
                                        for (line in block.lines) {
                                            allLines.add(line)
                                        }
                                    }

                                    val allHorizontalLineItems = makeHorizontalLineItems(allLines)

                                    val allHorizontalLineItemsConvertedToCustomLines = convertAllHorizontalLineItemsToCustomLineGroup(allHorizontalLineItems = allHorizontalLineItems)

                                    val today = Calendar.getInstance().time
                                    val currentTimeStamp = today.getStringTimeStampWithDate()

                                    var newBoardCaptureData = SingleBoardCaptureData(
                                            itemData = mutableListOf(),
                                            boardImageUriString = lastUriUsedForDetection.toString(),
                                            timestamp = currentTimeStamp,
                                            boardType = "",
                                            venueName = "",
                                            venueID = null,
                                            rectData = mutableListOf(),
                                            ocrData = allHorizontalLineItemsConvertedToCustomLines
                                    )

                                    groupingCompletion.invoke(newBoardCaptureData)
                                }
                            }
                            Log.i("Image", "Scaled bitmap height is ${scaledBitmap.height}")

                            // Picasso.get().load(dataUri).rotate(rotationDegrees.toFloat()).into(this.chooseGalleryTargetAfterSelection)
                        }
                    }
        }
    }

    private fun convertFirebaseLineToCustomLine(line: FirebaseVisionText.Line): CustomLine {
        return CustomLine(rect = convertFirebaseLineToCustomRect(line), text = line.text)
    }

    private fun convertFirebaseLineGroupIntoCustomLineGroup(lineGroup: List<FirebaseVisionText.Line>): List<CustomLine> {
        return lineGroup.map {
            convertFirebaseLineToCustomLine(it)
        }
    }

    private fun convertAllHorizontalLineItemsToCustomLineGroup(allHorizontalLineItems: MutableList<List<FirebaseVisionText.Line>>): MutableList<List<CustomLine>> {
        return allHorizontalLineItems.map {
            convertFirebaseLineGroupIntoCustomLineGroup(lineGroup = it)
        }.toMutableList()
    }

    private fun convertFirebaseLineToCustomRect(line: FirebaseVisionText.Line): CustomRect? {
        val boundingBox = line.boundingBox
        boundingBox?.let {
            return CustomRect(left = boundingBox.left, top = boundingBox.top, right = boundingBox.right, bottom = boundingBox.bottom)
        }
        return null
    }

    fun makeHorizontalLineItems(allLines: List<FirebaseVisionText.Line>): MutableList<List<FirebaseVisionText.Line>> {
        val allLinesGroupedByCloseVertical: MutableList<List<FirebaseVisionText.Line>> = mutableListOf()
        val sorted = sortAllLinesByVerticalAscending(allLines)
        // println(sorted)

        while (sorted.count() > 0) {
            val firstCloseMatchingVerticalSet = withVerticalSortedGroupGetFirstCloseVerticalMatchSet(sorted)
            if (firstCloseMatchingVerticalSet != null) {
                allLinesGroupedByCloseVertical.add(firstCloseMatchingVerticalSet)
                for (line in firstCloseMatchingVerticalSet) {
                    sorted.remove(line)
                }
            } else {
                sorted.removeAt(0)
            }
        }
        for (horizontalLineSet in allLinesGroupedByCloseVertical) {
            var lineText = ""
            for (line in horizontalLineSet) {
                lineText += line.text + ";"
            }
            Log.i("HorizontalLineText", lineText)
        }

        return allLinesGroupedByCloseVertical
        //  withFirstLineCheckForVerticalMatchesAndAddToList()
//        for (line in allLineItems) {
//            if line.second
//        }
    }

    fun withVerticalSortedGroupGetFirstCloseVerticalMatchSet(lineItems: List<FirebaseVisionText.Line>): List<FirebaseVisionText.Line>? {
        if (lineItems.count() < 2) return null
        val firstLine = lineItems.first()

        if (firstLine.boundingBox == null) {
            return null
        }

        val newVerticalLineGroup: MutableList<FirebaseVisionText.Line> = mutableListOf(firstLine)
        val lineItemCount = lineItems.count()
        for (x in 1..(lineItemCount - 1)) {
            val lineToCheck = lineItems[x]
            if (lineToCheck.boundingBox == null) {
                continue
            }

            val firstLineCenterY = firstLine.boundingBox!!.centerY()
            val thisLineCenterY = lineToCheck.boundingBox!!.centerY()

            if ((Math.abs(thisLineCenterY - firstLineCenterY)) < thresholdDifferenceInHeightForLineItemsMatch) {
                newVerticalLineGroup.add(lineToCheck)
            } else {
                break
            }
        }

        if (newVerticalLineGroup.count() >= 1) {
            val newVerticalGroupTextLowerCaseWithoutWhiteSpaceSeparatedBySpaces = newVerticalLineGroup.map {

                var replacedWhiteSpaceWithSpaceWord = replaceWhiteSpaceWith(it.text, " ")

                replacedWhiteSpaceWithSpaceWord.toLowerCase().split(" ")
            }.flatMap { it }

            val batProductKeyWordsLowerCaseSeparatedBySpaces = batProductKeyWords.map {
                it.toLowerCase().split(" ")
            }.flatMap { it }

            if (keyWordListContainsDetectedTextListForLineItem(batProductKeyWordsLowerCaseSeparatedBySpaces, newVerticalGroupTextLowerCaseWithoutWhiteSpaceSeparatedBySpaces)) {
                newVerticalLineGroup.sortBy { it.boundingBox!!.centerX() }
                return newVerticalLineGroup
            }
        }
        return null
    }

    private fun keyWordListContainsDetectedTextListForLineItem(keywords: List<String>, detectedWords: List<String>): Boolean {
        val chunkSize = 3
        for (keyword in keywords) {
            for (detectedWord in detectedWords) {
                if (detectedWord.contains(keyword)) {
                    return true
                }
                var chunkedDetectedWord = detectedWord.chunked(chunkSize)
                for (chunk in chunkedDetectedWord) {
                    if (chunk.count() >= chunkSize) {
                        if (keyword.contains(chunk)) {
                            return true
                        }
                    }
                }
            }
        }
        return false
    }

    fun sortAllLinesByVerticalAscending(lineItems: List<FirebaseVisionText.Line>): MutableList<FirebaseVisionText.Line> {
        val sortedAllLineItems = lineItems.toMutableList()
        sortedAllLineItems.sortWith(compareBy({ it.boundingBox!!.centerY() }))

        //  Log.i("LINE", "Bounding box first element is: ${sortedAllLineItems[0].boundingBox}")
        //   Log.i("LINE", "Corner points first element is: ${sortedAllLineItems[0].cornerPoints}")
        return sortedAllLineItems
    }
}