package com.pinkzebra.batboardreader.models

import com.google.android.gms.maps.model.LatLng

data class Venue(
        val venueID: Int,
        val venueName: String,
                   var isFavorite: Boolean,
                   val address: String,
                   val boardType: String? = null,
                   val latLng: LatLng)