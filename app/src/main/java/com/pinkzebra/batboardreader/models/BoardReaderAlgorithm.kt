package com.pinkzebra.batboardreader.models

import android.content.Context
import android.util.Log
import androidx.annotation.VisibleForTesting
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.label.FirebaseVisionOnDeviceAutoMLImageLabelerOptions
import com.marcinmoskala.math.times
import com.pinkzebra.batboardreader.MainActivity

import com.pinkzebra.batboardreader.utils.getStringTimeStampWithDate
import com.pinkzebra.batboardreader.utils.getUriFromString
import com.pinkzebra.batboardreader.utils.replaceWhiteSpaceWith
import java.util.*
import kotlin.math.roundToInt

const val AUTO_DETECT = "Auto Detect"
const val BOARD_TYPE_1 = "Brand | Size | Price | Size | Price"
const val BOARD_TYPE_2 = "Brand | Size | Pack | Carton"
const val BOARD_TYPE_3 = "Brand | Size | Price"
const val BOARD_TYPE_4 = "Product | Price | Carton"
const val BOARD_TYPE_5 = "Product & Size | Price"
const val BOARD_TYPE_6 = "Product & Size | Pack | Carton"
const val BOARD_TYPE_7 = "Product | Size | Pack | 2-Pack"

const val PACK = "Pack"
const val CARTON = "Carton"

class BoardReaderAlgorithm(activity: MainActivity?) {

    val PRODUCT_SIZES = listOf<Int>(20, 30, 25, 26, 50, 40, 15, 35, 32, 64, 80, 70, 52, 46, 23)
    val SUFFIXES_FOR_PRODUCT_SIZES = listOf<String>("s", "g", "'s", "", "6")

    var allowableProductSizeCombinations: List<String> = listOf()

    var batProductKeyWords: MutableList<String> = mutableListOf()

    val mlLabelToBoardTypeMap = mapOf<String, String>(
            "brand_pack_price" to BOARD_TYPE_3,
            "brand_size_price_size_price" to BOARD_TYPE_1,
            "brandsize_price" to BOARD_TYPE_5,
            "brandsize_pack_carton" to BOARD_TYPE_6,
            "brand_size_pack_carton" to BOARD_TYPE_2,
            "brand_size_pack_2pack" to BOARD_TYPE_3
    )

    init {
        activity?.let {
            batProductKeyWords = LocalRepository().getKeywordsJson(activity = activity).map { it.toLowerCase() }.toMutableList()
        }

        allowableProductSizeCombinations = (PRODUCT_SIZES.map {
            it.toString().toLowerCase()
        } * SUFFIXES_FOR_PRODUCT_SIZES).map { it.first.toLowerCase() + it.second.toLowerCase() }
    }

    fun doAutoBoardDetectIfNeededThenProcessOCR(spinnerString: String, singleBoardCaptureData: SingleBoardCaptureData, context: Context, algoCompletion: (SingleBoardCaptureData) -> Unit) {
        singleBoardCaptureData.ocrData?.let {
            val labelerOptions = FirebaseVisionOnDeviceAutoMLImageLabelerOptions.Builder()
                    .setLocalModelName("model")    // Skip to not use a local automl
                    .setConfidenceThreshold(0.4f)  // Evaluate your automl in the Firebase console
                    // to determine an appropriate value.
                    .build()
            val labeler = FirebaseVision.getInstance().getOnDeviceAutoMLImageLabeler(labelerOptions)

            if (spinnerString == AUTO_DETECT) {
                val imageUri = getUriFromString(singleBoardCaptureData.boardImageUriString)
                imageUri?.let {

                    val firebaseImage = FirebaseVisionImage.fromFilePath(context, imageUri)
                    labeler.processImage(firebaseImage)
                            .addOnSuccessListener { labels ->
                                Log.i("Vision", "Result for auto ml labeller: $labels")
                                for (label in labels) {
                                    Log.i("Vision", "One auto ml label: $label")
                                }

                                val labelWithMaxConfidence = labels.maxBy { it.confidence }
                                val labelText = labelWithMaxConfidence?.text
                                Log.i("Vision", "auto ml label with max confidence: $labelText")
                                labelText?.let {
                                    val boardType = mlLabelToBoardTypeMap[it]
                                    val processedBoardData = processBoardTypeWithAllOCRLines(boardType = boardType
                                            ?: "", allHorizontalLineItems = singleBoardCaptureData.ocrData.toMutableList())
                                    algoCompletion.invoke(singleBoardCaptureData.copy(itemData = processedBoardData.first.toMutableList(), rectData = processedBoardData.second, boardType = boardType
                                            ?: ""))
                                } ?: run {
                                    val boardType = BOARD_TYPE_3
                                    val processedBoardData = processBoardTypeWithAllOCRLines(boardType = boardType
                                            ?: "", allHorizontalLineItems = singleBoardCaptureData.ocrData.toMutableList())
                                    algoCompletion.invoke(singleBoardCaptureData.copy(itemData = processedBoardData.first.toMutableList(), rectData = processedBoardData.second, boardType = boardType
                                            ?: ""))
                                }
                            }
                            .addOnFailureListener { e ->
                                //TODO: Add failed auto detect handler, stop spinner
                                Log.i("Vision", "Result for auto ml labeller failed")
                            }
                }
            } else {
                val processedBoardData = processBoardTypeWithAllOCRLines(boardType = spinnerString
                        ?: "", allHorizontalLineItems = singleBoardCaptureData.ocrData.toMutableList())
                algoCompletion.invoke(singleBoardCaptureData.copy(itemData = processedBoardData.first.toMutableList(), rectData = processedBoardData.second.toList(), boardType = spinnerString
                        ?: ""))
            }
        } ?: run {
            algoCompletion.invoke(singleBoardCaptureData)
        }
    }

    private fun processBoardTypeWithAllOCRLines(boardType: String, allHorizontalLineItems: MutableList<List<CustomLine>>): Pair<List<ProductPrice>, MutableList<List<CustomRect?>>> {
        val boardTextData: MutableList<BoardTextItemModel> = mutableListOf()
        val latestProductPrices: MutableList<ProductPrice> = mutableListOf()
        val allRectData: MutableList<List<CustomRect?>> = mutableListOf()
        if (boardType in listOf<String>(BOARD_TYPE_1, BOARD_TYPE_2, BOARD_TYPE_3, BOARD_TYPE_5, BOARD_TYPE_6)) {
            when (boardType) {
                BOARD_TYPE_1 -> {
                    Log.i("BOARD", "Doing decode for board type 1")
                    // Brand | Size | Price | Size | Price

                    for (horiztonalLineGroup in allHorizontalLineItems) {
                        val rectGroup = convertHorizontalCustomLineToCustomRect(horiztonalLineGroup)
                        val productPossibleName = horiztonalLineGroup[0].text
                        val isPossibleProductNameMatch =
                                possibleProductNameMatchesKeywords(productPossibleName)

                        if (horiztonalLineGroup.count() == 5) {

                            val productPrice1 = convertPossibleTextPriceToFloat(
                                    horiztonalLineGroup[2].text
                            )
                            if (isPossibleProductNameMatch) {
                                val productSize = horiztonalLineGroup[1].text
                                if (productSize in allowableProductSizeCombinations && productPrice1 != null) {
                                    boardTextData.add(
                                            BoardTextItemModel(
                                                    productName = horiztonalLineGroup[0].text,
                                                    productPrice = productPrice1,
                                                    productSize = getGenericProductSize(
                                                            productSize
                                                    ),
                                                    packageType = PACK,
                                                    rectItems = rectGroup
                                            )
                                    )
                                }


                                val productPrice2 =
                                        convertPossibleTextPriceToFloat(
                                                horiztonalLineGroup[4].text
                                        )
                                val productSize2 = horiztonalLineGroup[3].text
                                if (productSize2 in allowableProductSizeCombinations && productPrice2 != null) {
                                    boardTextData.add(
                                            BoardTextItemModel(
                                                    productName = horiztonalLineGroup[0].text,
                                                    productPrice = productPrice2,
                                                    productSize = getGenericProductSize(
                                                            productSize2
                                                    ),
                                                    packageType = PACK,
                                                    rectItems = rectGroup
                                            )
                                    )
                                }

                            }
                        } else if (horiztonalLineGroup.count() <= 4) {
                            val sizeAndPriceArray = mutableListOf<String>()
                            horiztonalLineGroup.forEachIndexed { index, lineIndexed ->
                                if (index != 0) {
                                    val allTextItemsThisLine =
                                            lineIndexed.text.split(
                                                    " ",
                                                    ignoreCase = true
                                            )
                                    sizeAndPriceArray.addAll(
                                            allTextItemsThisLine
                                    )
                                }
                            }
                            if (sizeAndPriceArray.count() != 4) {
                                Log.i(
                                        "Price",
                                        "Suspicious price item inference"
                                )
                                Log.i(
                                        "Price",
                                        "Suspicious price item inference: ${horiztonalLineGroup[0].text} $sizeAndPriceArray"
                                )
                                if (sizeAndPriceArray.count() == 2) {
                                    if (!sizeAndPriceArray[0].contains('.') && sizeAndPriceArray[1].contains(
                                                    '.'
                                            )
                                    ) {
                                        val productSize = sizeAndPriceArray[0]
                                        val productPrice =
                                                convertPossibleTextPriceToFloat(
                                                        sizeAndPriceArray[1]
                                                )
                                        if (productSize in allowableProductSizeCombinations && productPrice != null && isPossibleProductNameMatch) {
                                            boardTextData.add(
                                                    BoardTextItemModel(
                                                            productName = horiztonalLineGroup[0].text,
                                                            productPrice = productPrice,
                                                            productSize = getGenericProductSize(
                                                                    productSize
                                                            ),
                                                            packageType = PACK,
                                                            rectItems = rectGroup
                                                    )
                                            )
                                        }
                                    }

                                }
                            } else {
                                Log.i("Price", "Good price item inference")
                                val productSize = sizeAndPriceArray[0]
                                val productPrice =
                                        convertPossibleTextPriceToFloat(
                                                sizeAndPriceArray[1]
                                        )
                                if (productSize in allowableProductSizeCombinations && productPrice != null && isPossibleProductNameMatch) {
                                    boardTextData.add(
                                            BoardTextItemModel(
                                                    productName = horiztonalLineGroup[0].text,
                                                    productPrice = productPrice,
                                                    productSize = getGenericProductSize(
                                                            productSize
                                                    ),
                                                    packageType = PACK,
                                                    rectItems = rectGroup
                                            )
                                    )
                                }

                                val productSize2 = sizeAndPriceArray[2]
                                val productPrice2 =
                                        convertPossibleTextPriceToFloat(
                                                sizeAndPriceArray[3]
                                        )
                                if (productSize2 in allowableProductSizeCombinations && productPrice2 != null && isPossibleProductNameMatch) {
                                    boardTextData.add(
                                            BoardTextItemModel(
                                                    productName = horiztonalLineGroup[0].text,
                                                    productPrice = productPrice2,
                                                    productSize = getGenericProductSize(
                                                            productSize2
                                                    ),
                                                    packageType = PACK,
                                                    rectItems = rectGroup
                                            )
                                    )
                                }
                            }
                        }
                    }
                }
                BOARD_TYPE_2 -> {
                    Log.i("BOARD", "Doing decode for board type 2")

                    // Brand | Size | Pack | Carton


                    for (line in allHorizontalLineItems) {
                        val rectGroup = convertHorizontalCustomLineToCustomRect(line)

                        val productPossibleName = line[0].text
                        val isPossibleProductNameMatch =
                                possibleProductNameMatchesKeywords(productPossibleName)

                        var productSize: String = ""
                        if (line.count() >= 2) {
                            productSize = line[1].text
                        }

                        if (line.count() == 3) {

                            val possiblePackPriceText = line[2].text

                            var indexesOfDollarFound = mutableListOf<Int>()

                            var countOfDollarsInText: Int = 0
                            possiblePackPriceText.forEachIndexed { index, char ->
                                if (char == '$') {
                                    countOfDollarsInText++
                                    indexesOfDollarFound.add(index)
                                }
                            }

                            var possiblePackPrice: Float? = null
                            var possibleCartonPrice: Float? = null

                            if (countOfDollarsInText == 1) {
                                possiblePackPrice =
                                        convertPossibleTextPriceToFloat(possiblePackPriceText)
                            } else if (countOfDollarsInText == 2) {
                                val packPriceString = possiblePackPriceText.substring(0, indexesOfDollarFound[1])
                                possiblePackPrice =
                                        convertPossibleTextPriceToFloat(packPriceString)

                                val cartonPriceString = possiblePackPriceText.substring(indexesOfDollarFound[1])
                                possibleCartonPrice =
                                        convertPossibleTextPriceToFloat(cartonPriceString)
                            }

                            if (productSize in allowableProductSizeCombinations && isPossibleProductNameMatch) {
                                if (possiblePackPrice != null) {
                                    boardTextData.add(
                                            BoardTextItemModel(
                                                    productName = line[0].text,
                                                    productPrice = possiblePackPrice,
                                                    productSize = line[1].text,
                                                    packageType = PACK,
                                                    rectItems = rectGroup
                                            )
                                    )
                                }

                                if (possibleCartonPrice != null) {
                                    boardTextData.add(
                                            BoardTextItemModel(
                                                    productName = line[0].text,
                                                    productPrice = possibleCartonPrice,
                                                    productSize = line[1].text,
                                                    packageType = CARTON,
                                                    rectItems = rectGroup
                                            )
                                    )
                                }
                            }
                        }

                        if (line.count() == 4) {
                            val productPrice1 =
                                    convertPossibleTextPriceToFloat(line[2].text)
                            if (productSize in allowableProductSizeCombinations && productPrice1 != null && isPossibleProductNameMatch) {
                                boardTextData.add(
                                        BoardTextItemModel(
                                                productName = line[0].text,
                                                productPrice = productPrice1,
                                                productSize = line[1].text,
                                                packageType = PACK,
                                                rectItems = rectGroup
                                        )
                                )

                                val productPrice2 =
                                        convertPossibleTextPriceToFloat(
                                                line[3].text
                                        )
                                productPrice2?.let {
                                    boardTextData.add(
                                            BoardTextItemModel(
                                                    productName = line[0].text,
                                                    productPrice = productPrice2,
                                                    productSize = line[1].text,
                                                    packageType = CARTON,
                                                    rectItems = rectGroup
                                            )
                                    )
                                }
                            }
                        }
                    }
                }

                BOARD_TYPE_3 -> {
                    for (line in allHorizontalLineItems) {
                        val rectGroup = convertHorizontalCustomLineToCustomRect(line)
                        val combinedString = convertHorizontalLineItemListToSingleString(line)

                        val boardTextItemModel = withSingleStringProcessBoardType3(combinedString)
                        boardTextItemModel?.let {
                            boardTextData.add(boardTextItemModel.copy(packageType = PACK, rectItems = rectGroup))
                        }

                    }
                }

                BOARD_TYPE_4 -> {
                    for (line in allHorizontalLineItems) {

                        val rectGroup = convertHorizontalCustomLineToCustomRect(line)
                        if (line.count() == 3 || line.count() == 2) {
                            var productItem = line[0].text.toLowerCase()
                            var productSizeFound: String? = null
                            for (productSize in allowableProductSizeCombinations) {
                                if (productItem.contains(productSize)) {
                                    productSizeFound = productSize
                                }
                            }

                            var priceItems = mutableListOf<Float?>(null, null)
                            if (line.count() == 2) {
                                val splitText = line[1].text.split(' ')
                                if (splitText.count() == 2) {
                                    priceItems[0] =
                                            convertPossibleTextPriceToFloat(
                                                    splitText[0]
                                            )
                                    priceItems[1] =
                                            convertPossibleTextPriceToFloat(
                                                    splitText[1]
                                            )
                                }
                            } else {
                                priceItems[0] =
                                        convertPossibleTextPriceToFloat(line[1].text)
                                priceItems[1] =
                                        convertPossibleTextPriceToFloat(line[2].text)
                            }
                            productSizeFound?.let { productSize ->
                                val productName = line[0].text.replace(
                                        productSize,
                                        "",
                                        ignoreCase = true
                                )

                                val isPossibleProductNameMatch =
                                        possibleProductNameMatchesKeywords(productName)

                                val productPrice1 = priceItems[0]

                                val genericProductSize =
                                        getGenericProductSize(productSize)

                                if (isPossibleProductNameMatch && productPrice1 != null) {
                                    boardTextData.add(
                                            BoardTextItemModel(
                                                    productName = productName,
                                                    productPrice = productPrice1,
                                                    productSize = genericProductSize,
                                                    packageType = PACK,
                                                    rectItems = rectGroup
                                            )
                                    )
                                }

                                val productPrice2 = priceItems[1]

                                if (isPossibleProductNameMatch && productPrice2 != null) {
                                    boardTextData.add(
                                            BoardTextItemModel(
                                                    productName = productName,
                                                    productPrice = productPrice2,
                                                    productSize = genericProductSize,
                                                    packageType = CARTON,
                                                    rectItems = rectGroup
                                            )
                                    )
                                }
                            }
                        }
                    }
                }
                BOARD_TYPE_5 -> {
                    for (line in allHorizontalLineItems) {
                        val rectGroup = convertHorizontalCustomLineToCustomRect(line)
                        if (line.count() == 2) {
                            var productItem = line[0].text.toLowerCase()
                            var productSizeFound: String? = null
                            for (productSize in allowableProductSizeCombinations) {
                                if (productItem.contains(productSize)) {
                                    if (productSize.count() > (productSizeFound
                                                    ?: "").count()
                                    ) {
                                        productSizeFound = productSize
                                    }
                                }
                            }

                            var price: Float? = null
                            if (line.count() == 2) {
                                val text = line[1].text
                                price =
                                        convertPossibleTextPriceToFloat(text)
                            }
                            productSizeFound?.let { productSize ->
                                var productName = line[0].text.replace(
                                        productSize,
                                        "",
                                        ignoreCase = true
                                )

                                if (productName.last() == ' ') {
                                    productName = productName.dropLast(1)
                                }

                                val isPossibleProductNameMatch =
                                        possibleProductNameMatchesKeywords(productName)
                                if (isPossibleProductNameMatch && price != null) {
                                    boardTextData.add(
                                            BoardTextItemModel(
                                                    productName = productName,
                                                    productPrice = price,
                                                    productSize = getGenericProductSize(
                                                            productSize
                                                    ),
                                                    packageType = PACK,
                                                    rectItems = rectGroup
                                            )
                                    )
                                }
                            }
                        }
                    }
                }

                BOARD_TYPE_6 -> {
                    for (line in allHorizontalLineItems) {

                        val rectGroup = convertHorizontalCustomLineToCustomRect(line)
                        if (line.count() == 3 || line.count() == 2) {
                            var productItem = line[0].text.toLowerCase()
                            var productSizeFound: String? = null
                            for (productSize in allowableProductSizeCombinations) {
                                if (productItem.contains(productSize)) {
                                    if (productSize.count() > (productSizeFound
                                                    ?: "").count()
                                    ) {
                                        productSizeFound = productSize
                                    }
                                }
                            }

                            var possiblePackPrice: Float? = null
                            var possibleCartonPrice: Float? = null

                            val possiblePackPriceText = line[1].text

                            var indexesOfDollarFound = mutableListOf<Int>()

                            var countOfDollarsInText: Int = 0
                            possiblePackPriceText.forEachIndexed { index, char ->
                                if (char == '$') {
                                    countOfDollarsInText++
                                    indexesOfDollarFound.add(index)
                                }
                            }

                            if (countOfDollarsInText == 1) {
                                possiblePackPrice =
                                        convertPossibleTextPriceToFloat(possiblePackPriceText)
                            } else if (countOfDollarsInText == 2) {
                                val packPriceString = possiblePackPriceText.substring(0, indexesOfDollarFound[1])
                                possiblePackPrice =
                                        convertPossibleTextPriceToFloat(packPriceString)

                                val cartonPriceString = possiblePackPriceText.substring(indexesOfDollarFound[1])
                                possibleCartonPrice =
                                        convertPossibleTextPriceToFloat(cartonPriceString)
                            }

                            if (line.count() >= 3) {
                                val possibleCartonPriceText = line[2].text
                                val possibleCarbonPriceInThirdColumnCSV = convertPossibleTextPriceToFloat(possibleCartonPriceText)
                                if (possibleCarbonPriceInThirdColumnCSV != null) {
                                    possibleCartonPrice = possibleCarbonPriceInThirdColumnCSV
                                }

                            }
                            productSizeFound?.let { productSize ->
                                var productName = line[0].text.replace(
                                        productSize,
                                        "",
                                        ignoreCase = true
                                )

                                if (productName.last() == ' ') {
                                    productName = productName.dropLast(1)
                                }

                                val isPossibleProductNameMatch =
                                        possibleProductNameMatchesKeywords(productName)
                                if (isPossibleProductNameMatch && possiblePackPrice != null) {
                                    boardTextData.add(
                                            BoardTextItemModel(
                                                    productName = productName,
                                                    productPrice = possiblePackPrice,
                                                    productSize = getGenericProductSize(
                                                            productSize
                                                    ),
                                                    packageType = PACK,
                                                    rectItems = rectGroup
                                            )
                                    )
                                }

                                if (isPossibleProductNameMatch && possibleCartonPrice != null) {
                                    boardTextData.add(
                                            BoardTextItemModel(
                                                    productName = productName,
                                                    productPrice = possibleCartonPrice,
                                                    productSize = getGenericProductSize(
                                                            productSize
                                                    ),
                                                    packageType = CARTON,
                                                    rectItems = rectGroup
                                            )
                                    )
                                }
                            }
                        }
                    }
                }
            }

            if (boardTextData.count() >= 0) {
                val productPrices = boardTextData.map {
                    ProductPrice(
                            productName = it.productName,
                            productSize = getGenericProductSize(it.productSize),
                            price = it.productPrice,
                            packageType = it.packageType
                    )
                }
                latestProductPrices.addAll(productPrices)
//                                                    val currentTimeStamp =
//                                                        DateTimeFormatter.ISO_INSTANT.format(Instant.now())

                val rectData = boardTextData.mapNotNull {
                    it.rectItems
                }

                allRectData.addAll(rectData)
            }
        }
        return Pair(latestProductPrices, allRectData)
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun withSingleStringProcessBoardType3(string: String): BoardTextItemModel? {
        var conditionedString = string.toLowerCase()
        val exceptionTextList = listOf<String>("93mm")

        for (exceptionText in exceptionTextList) {
            val replacementText = " ".repeat(exceptionText.length)
            conditionedString = conditionedString.replace(exceptionText,replacementText, ignoreCase = true)
        }

        val detectedBoardTextItem: BoardTextItemModel? = null
        var startIndexFirstDigit: Int? = null
        var firstNonDigitIndexAfterInt: Int? = null

        var counter = 0
        for (char in conditionedString) {
            if (char.isDigit() && startIndexFirstDigit == null) {
                startIndexFirstDigit = counter

            }
            if (!char.isDigit() && startIndexFirstDigit != null && firstNonDigitIndexAfterInt == null) {
                firstNonDigitIndexAfterInt = counter

            }

            if (startIndexFirstDigit != null && firstNonDigitIndexAfterInt != null) {
                if ((firstNonDigitIndexAfterInt - startIndexFirstDigit) == 1) {
                    startIndexFirstDigit = null
                    firstNonDigitIndexAfterInt = null
                }
            }

            counter++
        }

        if (startIndexFirstDigit != null && firstNonDigitIndexAfterInt != null) {
            val possibleSizeString = string.substring(startIndexFirstDigit, firstNonDigitIndexAfterInt + 1)

            var possibleProductSizes =
                    allowableProductSizeCombinations.filter {
                        possibleSizeString.toLowerCase()
                                .contains(it)
                    }

            var possibleProductSizesWithMaxCharacterCount =
                    possibleProductSizes.maxBy { it.length }

            val possiblePriceString = string.substring(firstNonDigitIndexAfterInt + 1)

            val possiblePrice = convertPossibleTextPriceToFloat(possiblePriceString)

            var possibleProductName = string.substring(0, startIndexFirstDigit)

            if (!possibleProductName.isEmpty()) {
                if (possibleProductName.last() == ' ') {
                    possibleProductName = possibleProductName.dropLast(1)
                }
                possiblePrice?.let {
                    return BoardTextItemModel(productSize = possibleProductSizesWithMaxCharacterCount
                            ?: "", packageType = "", productName = possibleProductName, productPrice = possiblePrice)
                }
            }

        }
        return detectedBoardTextItem
    }

    private fun convertHorizontalLineItemListToSingleString(list: List<CustomLine>): String {
        var combinedString = ""

        for (line in list) {
            combinedString += line.text + " "
        }

        return combinedString
    }

    private fun convertHorizontalCustomLineToCustomRect(lines: List<CustomLine>): List<CustomRect?> {
        return lines.map {
            it.rect
        }
    }

    private fun possibleProductNameMatchesKeywords(word: String): Boolean {
        var replacedWhiteSpaceWithSpaceWord: String = replaceWhiteSpaceWith(word, " ")

        val spaceSeparatedWords = replacedWhiteSpaceWithSpaceWord.split(" ").map { it.toLowerCase() }
        val result = spaceSeparatedWords.intersect(batProductKeyWords)
        if (result.count() > 0) {
            return true
        } else {
            return false
        }
    }

    private fun getGenericProductSize(productSize: String): String {
        val lowerCaseProductSize = productSize.toLowerCase()
        var output: String = lowerCaseProductSize
        output = output.replace("'s", "")
        output = output.replace("s", "")
        output = output.replace("6", "G")
        return output
    }

    fun convertPossibleTextPriceToFloat(text: String): Float? {
        var conditionedText = text
        conditionedText = conditionedText.replace("S", "$", true)
        conditionedText = conditionedText.replace("$", "", true)
        conditionedText = conditionedText.replace(" ", "", ignoreCase = true)
        conditionedText = conditionedText.replace(",", ".", ignoreCase = true)

        val decimalCount = conditionedText.count {
            it == '.'
        }

        var possibleFloat: Float?
        if (decimalCount == 2) {
            val indexOfLastDecimal = conditionedText.indexOfLast {
                it == '.'
            }

            val trimSecondDecimalString = conditionedText.replaceRange(indexOfLastDecimal, indexOfLastDecimal + 1, "")
            possibleFloat = trimSecondDecimalString.toFloatOrNull()
        } else {
            possibleFloat = conditionedText.toFloatOrNull()
        }

        if (possibleFloat == null) {
            return null
        } else {
            val roundedTwoDecimalsFloat = ((possibleFloat * 100).roundToInt().toFloat()) / 100
            return roundedTwoDecimalsFloat
        }


    }

    private fun preconditionLineItems(oneLineArray: List<CustomLine>): List<String> {
        val lineTextConditioned = oneLineArray.map {
            it.text.replace("$ ", "$")
        }
        return lineTextConditioned
    }
}