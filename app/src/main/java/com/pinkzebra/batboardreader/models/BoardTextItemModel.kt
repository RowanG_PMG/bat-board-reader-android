package com.pinkzebra.batboardreader.models

import android.graphics.Rect
import java.io.Serializable

data class BoardTextItemModel(val productID: Int? = null,val manufacturerID: Int? = null, val productName: String, val productPrice: Float, val productSize: String, val packageType: String, val priceDifference: Float?=null, val rectItems: List<CustomRect?>? = null, var wasAutoMapped: Boolean? = null, var isCompliant: Boolean?= null): Serializable

data class  BoardTextModel(val boardPricingData: List<BoardTextItemModel>, val boardImageUriString: String): Serializable

data class AllVenueBoardHistory(val history: BoardTextModel)

