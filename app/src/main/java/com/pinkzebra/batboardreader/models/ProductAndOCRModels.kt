package com.pinkzebra.batboardreader.models

import android.graphics.Rect
import java.io.Serializable

data class ProductPrice (var productName: String, var productSize: String, var manufacturerID: Int? = null,var packageType: String, var genericPackageType: String? = null,var price: Float, var productID: Int? = null, val wasAutoMapped: Boolean? = null, val isCompliant: Boolean? = null): Serializable

data class Product(val name: String, val size: String, val packageType: String, val lookup: List<String>?=null, val manufacturerID: Int? = null, val productID: Int? = null, val packageType_id: Int? = null)

data class SingleBoardCaptureData(var itemData: MutableList<ProductPrice>, val boardImageUriString: String, val boardImageRemoteFilename: String? = null,val timestamp: String, val venueName: String, val boardType: String, val rectData: List<List<CustomRect?>>? = null, val venueID: Int? = null, val ocrData: List<List<CustomLine>>? = null): Serializable

data class  CustomRect(val left: Int, val top: Int, val right: Int, val bottom: Int): Serializable

data class  CustomLine(val rect: CustomRect?, val text: String): Serializable


data class BoardCaptureDataHistory(val itemsHistory: MutableList<SingleBoardCaptureData>): Serializable

