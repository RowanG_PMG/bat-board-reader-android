package com.pinkzebra.batboardreader.models

data class Manufacturer(val name: String, val id: Int)