package com.pinkzebra.batboardreader.models

import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.graphics.drawable.Drawable
import android.net.Uri
import android.util.Log
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.GeoPoint
import com.google.firebase.storage.FirebaseStorage
import com.google.gson.Gson
import android.widget.ImageView
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.google.common.io.ByteStreams
import com.pinkzebra.batboardreader.fragments.VenueMappedPricingFragment
import com.pinkzebra.batboardreader.utils.GlideApp
import com.pinkzebra.batboardreader.utils.convertTimeZoneStampToUnixTimeStamp
import java.util.*
import kotlin.collections.HashMap


class FirestoreRepository {

    data class ImportedVenueModelStructure(
        val id: String?,
        val team: String?,
        val teamID: Int?,
        val storeNumber: Int?,
        val name: String?,
        val account: String?,
        val address: String?,
        val suburb: String?,
        val postcode: Int?,
        val state: String?,
        val phone: String?,
        val boardType: String?,
        val latitude: Float?,
        val longitude: Float?
    )

    data class ImportedProductModelStructure(
        val product_id: Int?,
        val name: String?,
        val size: String?,
        val size_id: Int?,
        val packageType: String?,
        val packageType_id: Int?,
        val brand: String?,
        val manufacturer: String?,
        val manufacturer_id: Int?,
        val lookup: String?
    )

    fun uploadImageToFirestore(
        imageUri: Uri,
        filename: String,
        successHandler: (() -> Unit)? = null,
        completionHandler: (() -> Unit)? = null
    ) {
        val storage = FirebaseStorage.getInstance()
        val storageRef = storage.reference
        val imagesRef = storageRef.child("board_images")
        val fileName = filename
        val newBoardImageRef = imagesRef.child(fileName)

        //   var file = Uri.fromFile(File(imageUri.path))

        val uploadTask = newBoardImageRef.putFile(imageUri)

        uploadTask.addOnFailureListener {
            // Handle unsuccessful uploads
        }.addOnSuccessListener {
            Log.i("Firestore", "Image successfully uploaded")
        }.addOnCompleteListener {
            completionHandler?.invoke()
        }
    }

    fun getBoardImageFromFirebase(
        filename: String,
        context: Context,
        imageView: ImageView,
        completionHandler: ((width: Int?, height: Int?) -> Unit)?
    ) {
        val storage = FirebaseStorage.getInstance()
        val storageRef = storage.reference
        val imageRef = storageRef.child("board_images/$filename")

        GlideApp.with(context)
            .load(imageRef).override(Target.SIZE_ORIGINAL)
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    p0: GlideException?,
                    p1: Any?,
                    p2: com.bumptech.glide.request.target.Target<Drawable>?,
                    p3: Boolean
                ): Boolean {
                    Log.i("Firestore", "On Load failed")
                    completionHandler?.invoke(null, null)
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: com.bumptech.glide.load.DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    val originalWidth = resource?.intrinsicWidth
                    val originalHeight = resource?.intrinsicHeight
                    if (originalHeight != null && originalWidth != null && originalHeight != 0 && originalWidth != 0) {
                        completionHandler?.invoke(originalWidth, originalHeight)
                    } else {
                        Log.i("Firestore", "0 dimensions image")
                        completionHandler?.invoke(null, null)
                    }
                    return false
                }

            }).fitCenter().into(imageView)
    }

    fun fetchBoardHistoryFirebase(
        venueID: Int,
        completionHandler: ((List<SingleBoardCaptureData>) -> Unit)?,
        failHandler: ((List<SingleBoardCaptureData>) -> Unit)? = null
    ) {
        val db = FirebaseFirestore.getInstance()

        db.collection("board_data").whereEqualTo("venueID", venueID)
            .get()
            .addOnSuccessListener { result ->
                val gson = Gson()

                val previousProductPrices = result.map {
                    val data = it.data
                    val jsonData = data["json_data"] as String
                    val productPrice = gson.fromJson(jsonData, SingleBoardCaptureData::class.java)
                    productPrice
                }.sortedBy { convertTimeZoneStampToUnixTimeStamp(it.timestamp) }
                completionHandler?.invoke(previousProductPrices)
            }
            .addOnFailureListener { exception ->
                Log.w(ContentValues.TAG, "Error getting documents.", exception)
            }
    }

    fun fetchVenueData(
        teamID: Int,
        completionHandler: ((List<Venue>) -> Unit)? = null,
        failHandler: (() -> Unit)? = null
    ) {

        val db = FirebaseFirestore.getInstance()

        db.collection("venues").whereEqualTo("team_id", teamID)
            .get()
            .addOnSuccessListener { result ->

                val venues = result.mapNotNull { item ->
                    val data = item.data
                    val address = data["address"] as String?
                    val name = data["name"] as String?
                    val storeNumber = (data["storeNumber"] as Long?)?.toInt()
                    val location = data["location"] as GeoPoint
                    val latLng = com.google.android.gms.maps.model.LatLng(
                        location.latitude,
                        location.longitude
                    )

                    storeNumber?.let {
                        val venue = Venue(
                            venueID = storeNumber, venueName = name
                                ?: "", isFavorite = false, address = address
                                ?: "", latLng = latLng
                        )
                        venue
                    }
                }
                completionHandler?.invoke(venues)
            }.addOnFailureListener { exception ->
                Log.w(ContentValues.TAG, "Error getting documents.", exception)
            }

    }

    fun fetchProductData(
        completionHandler: ((List<Product>) -> Unit)? = null,
        failHandler: (() -> Unit)? = null
    ) {
        val db = FirebaseFirestore.getInstance()

        db.collection("products")
            .get()
            .addOnSuccessListener { result ->
                val products = result.map { item ->
                    val data = item.data
                    val size = data["size"] as String
                    val name = data["name"] as String
                    val packageType = data["packageType"] as String
                    val lookupArray = data["lookup"] as List<String>
                    val manufacturer_id = (data["manufacturer_id"] as Long).toInt()
                    val product_id = (data["product_id"] as Long).toInt()
                    val packageTypeID = (data["packageType_id"] as Long).toInt()
                    Product(
                        name = name,
                        size = size,
                        packageType = packageType,
                        lookup = lookupArray,
                        manufacturerID = manufacturer_id,
                        productID = product_id,
                        packageType_id = packageTypeID
                    )
                }
                completionHandler?.invoke(products)
            }.addOnFailureListener { exception ->
                Log.w(ContentValues.TAG, "Error getting documents.", exception)
            }
    }

    fun fetchVenuesData(
        completionHandler: ((List<Product>) -> Unit)? = null,
        failHandler: (() -> Unit)? = null
    ) {

        val db = FirebaseFirestore.getInstance()

        db.collection("venues")
            .get()
            .addOnSuccessListener { result ->

            }.addOnFailureListener { exception ->
                Log.w(ContentValues.TAG, "Error getting documents.", exception)
            }

    }

    fun fetchVenuesRecentPricesMatchingProductIDAndTeamVenues(
        productID: Int,
        teamVenueIDs: List<Int>,
        completionHandler: ((List<VenueMappedPricingFragment.VenueProductPrice>) -> Unit)? = null,
        failHandler: (() -> Unit)? = null
    ) {
        val db = FirebaseFirestore.getInstance()
        val currentTimestamp = Date().time
        db.collection("board_data")
            .get()
            .addOnSuccessListener { result ->
                val gson = Gson()

                val allBoardData = result.map {
                    val data = it.data
                    val jsonData = data["json_data"] as String
                    val productPrice = gson.fromJson(jsonData, SingleBoardCaptureData::class.java)
                    productPrice
                }

                val recentData = allBoardData.filter {
                    (currentTimestamp - convertTimeZoneStampToUnixTimeStamp(it.timestamp)) <= 60 * 24 * 60 * 60 * 1000L
                }

                val recentBoardDataForTeam = recentData.filter {
                    if (teamVenueIDs.contains(it.venueID)) true else false
                }

                val venueGroupedBoardData = recentBoardDataForTeam.groupBy { it.venueID }

                val mostRecentSingleBoardDataAtVenue = venueGroupedBoardData.mapNotNull {
                    val key = it.key
                    val maxInList = it.value.maxBy { it.timestamp }
                    if (key != null && maxInList != null) {
                        Pair(key, maxInList)
                    } else {
                        null
                    }
                }.toMap()

                val mostRecentPriceAtVenuesMatchingProductID =
                    mostRecentSingleBoardDataAtVenue.mapNotNull {
                        val key = it.key
                        val productMatcingProductID =
                            it?.value.itemData?.firstOrNull { it.productID == productID }
                        if (key != null && productMatcingProductID != null) {
                            Pair(key, productMatcingProductID)
                        } else {
                            null
                        }
                    }.toMap()
                val venuePriceMatchingProductID = mostRecentPriceAtVenuesMatchingProductID.map {
                    VenueMappedPricingFragment.VenueProductPrice(it.key, price = it.value.price)
                }
                completionHandler?.invoke(venuePriceMatchingProductID)
            }
            .addOnFailureListener { exception ->
                Log.w(ContentValues.TAG, "Error getting documents.", exception)
            }

    }

    fun fetchVenueBoardData(venueID: Int) {
        val db = FirebaseFirestore.getInstance()

        db.collection("board_data").whereEqualTo("storeNumber", venueID)
            .get()
            .addOnSuccessListener { result ->
                Log.i("VenueData", result.toString())
            }.addOnFailureListener { exception ->
                Log.w(ContentValues.TAG, "Error getting documents.", exception)
            }
    }


    fun uploadAllVenueJSONDataToFirestore(activity: Activity) {
        val gson = Gson()
        val inputStream = activity!!.assets.open("batdata/venues_data.json")
        val jsonVenues = String(ByteStreams.toByteArray(inputStream))

        val jsonVenuesObject =
            gson.fromJson(jsonVenues, Array<ImportedVenueModelStructure>::class.java)
        Log.i("Firestore", jsonVenues.toString())
        Log.i("Firestore", "Count of venues to upload: " + jsonVenuesObject.count())
        val db = FirebaseFirestore.getInstance()

        for (venue in jsonVenuesObject) {
            val items = HashMap<String, Any?>()
            items.put("id", venue.id)
            items.put("team", venue.team)
            items.put("team_id", venue.teamID)
            items.put("storeNumber", venue.storeNumber)
            items.put("name", venue.name)
            items.put("account", venue.account)
            items.put("suburb", venue.suburb)
            items.put("postcode", venue.postcode)
            items.put("address", venue.address)
            val latitude = venue.latitude?.toDouble() ?: 0.0
            val longitude = venue.longitude?.toDouble() ?: 0.0
            val location = GeoPoint(latitude, longitude)
            items.put("location", location)
            db.collection("venues")
                .add(items)
        }
    }

    fun uploadAllProductJSONDataToFirestore(activity: Activity) {
        val gson = Gson()
        val inputStream = activity!!.assets.open("batdata/product_data.json")
        val jsonProducts = String(ByteStreams.toByteArray(inputStream))

        val jsonProductsObject =
            gson.fromJson(jsonProducts, Array<ImportedProductModelStructure>::class.java)
        Log.i("Firestore", jsonProducts.toString())
        Log.i("Firestore", "Count of venues to upload: " + jsonProductsObject.count())
        val db = FirebaseFirestore.getInstance()

        for (products in jsonProductsObject) {
            val items = HashMap<String, Any?>()
            items.put("product_id", products.product_id)
            items.put("name", products.name)
            items.put("size", products.size)
            items.put("size_id", products.size_id)
            items.put("packageType", products.packageType)
            items.put("packageType_id", products.packageType_id)
            items.put("manufacturer_id", products.manufacturer_id)

            val lookupCSV = products.lookup
            val lookupArray = lookupCSV?.split(",", ignoreCase = true) ?: listOf()

            items.put("lookup", lookupArray)
            db.collection("products")
                .add(items)
        }
    }


}